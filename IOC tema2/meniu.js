function meniu(parent) {
	this.div = document.createElement("div");
	this.div.className = "divmenu";

	//create first component
	this.settings = document.createElement("img");
	this.settings.src = "ic_action_settings.png";
	this.settings.className = "settings";
	this.settings.id = "settings";
	this.settings.style.left = "30px";
	this.settings.setAttribute( "onClick", "javascript: clicksettings(this.id);" );

	this.home = document.createElement("img");
	this.home.src = "home.png";
	this.home.className = "settings";
	this.home.id = "home";
	this.home.style.left = "150px";
	this.home.setAttribute( "onClick", "javascript: clickhome(this.id);" );

	this.search = document.createElement("img");
	this.search.src = "ic_action_search.png";;
	this.search.className = "settings";
	this.search.id = "search";
	this.search.style.right = "150px";
	this.search.setAttribute( "onClick", "javascript: clicksearch(this.id);" );

	this.back = document.createElement("img");
	this.back.src = "ic_action_back.png";
	this.back.className = "settings";
	this.back.id = "back";
	this.back.style.right = "30px";
	this.back.setAttribute( "onClick", "javascript: clickback(this.id); ");
	
	this.parent = parent;

	this.div.appendChild(this.settings);
	this.div.appendChild(this.home);
	this.div.appendChild(this.search);
	this.div.appendChild(this.back);

	this.parent.appendChild(this.div);
}

function tastatura(parent) {
	this.parent = parent;
	this.div = document.createElement("div");
	this.div.className = "tastatura";
	this.input = document.createElement("div");
	this.input.className = "input";

	this.search_text = document.createElement("input");
	this.search_text.type = "text";
	this.search_text.className = "search_text";
	this.search_button = document.createElement("input");
	this.search_button.type = "submit";
	this.search_button.value = "Search";
	this.search_button.className = "search_button";
	this.search_button.setAttribute( "onClick", "javascript: clickbuttonsearch(this.id);" );
	this.input.appendChild(this.search_text);
	this.input.appendChild(this.search_button);
	this.div.appendChild(this.input);
	this.parent.appendChild(this.div);
}

function content_ziare(parent) {
	this.parent = parent;
	this.div = document.createElement("div");
	this.div.id = "contentziare";
	this.div.className = "contentziare";
	this.parent.appendChild(this.div);
}

function detalii_comanda(parent) {
	this.parent = parent;
	this.div = document.createElement("div");
	this.div.id = "detalii";
	this.div.className = "detalii";
	this.buttonSelect = document.createElement("a");
	this.buttonSelect.className = "buttonAdauga";
	this.buttonSelect.style.marginBottom = "0px";
	this.buttonSelect.style.width = "200px";
	this.buttonSelect.style.height = "50px";
	this.buttonSelect.setAttribute( "onClick", "history.go(0)" );
	this.buttonSelect.innerHTML = "Finalizare";
	this.buttonSelect.style.marginRight = "0px";
	this.div.appendChild(this.buttonSelect);
	this.parent.appendChild(this.div);
	
}