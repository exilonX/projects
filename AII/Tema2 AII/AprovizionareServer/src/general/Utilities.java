package general;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import databaseconnection.DataBaseConnection;

public class Utilities {
    public static boolean isTableName(String entry) {
        try {
            for (String tableName:DataBaseConnection.getTableNames()) {
                if (entry.toLowerCase().replaceAll(" ","").equals(tableName.toLowerCase().replaceAll("_",""))) {    
                    return true;
                }    
            }
        } catch (Exception exception) {
            System.out.println ("Exceptie: "+exception.getMessage());
        }
        return false;
    }
    
    public static ArrayList<String> select_cols(String[] cols, ArrayList<String> allcols) {
    	ArrayList<String> ret = new ArrayList<>();
    	for (String col : cols) {
    		if (allcols.contains(col)) {
    			ret.add(col);
    		}
    	}
    	return ret;
    }
    
    public static ArrayList<String> select_cols_index(Integer[] cols, ArrayList<String> allcols) {
    	ArrayList<String> ret = new ArrayList<>();
    	for (Integer col : cols) {
    		ret.add(allcols.get(col));
    	}
    	return ret;
    }
    
    public static String max_dates(String date1, String date2){
    	String[] date1_split = date1.split("-");
    	String[] date2_split = date2.split("-");
    	GregorianCalendar d1 = new GregorianCalendar(Integer.parseInt(date1_split[0]), 
    			Integer.parseInt(date1_split[1]), Integer.parseInt(date1_split[2]));
    	GregorianCalendar d2 = new GregorianCalendar(Integer.parseInt(date2_split[0]), 
    			Integer.parseInt(date2_split[1]), Integer.parseInt(date2_split[2]));
    	if (d1.compareTo(d2) < 0) {
    		return date2;
    	} else {
    		return date1;
    	}
    }
    
    public static String add_to_date(String date, int nr_days) {
    	String[] datesplit = date.split("-");

    	Calendar d = new GregorianCalendar(Integer.parseInt(datesplit[0]), 
    			Integer.parseInt(datesplit[1]), Integer.parseInt(datesplit[2]));

    	d.add(GregorianCalendar.DAY_OF_MONTH, nr_days);
    	
    	SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");

        fmt.setCalendar(d);

        String dateFormatted = fmt.format(d.getTime());
    	return dateFormatted;
    }
    
    public static String add_to_dateok(String date, int nr_days) {
    	System.out.println("Data la care trebuie sa adaug este" + date);
    	String[] datesplit = date.split("-");
    	
    	String new_date = datesplit[0] + "-"+ String.valueOf(Integer.parseInt(datesplit[1]) - 1) +
    			"-"  + datesplit[2];
    	System.out.println("New date" + new_date);
    	String d = add_to_date(new_date, nr_days);
    	System.out.println("add to date " + d);
    	datesplit = d.split("-");
    	return datesplit[0] + "-"+ String.valueOf(Integer.parseInt(datesplit[1]) + 1) +
    			"-" + datesplit[2];
    }
    
    
    
    
}
