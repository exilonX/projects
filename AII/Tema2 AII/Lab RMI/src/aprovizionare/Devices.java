package aprovizionare;

import java.io.Serializable;

public class Devices implements Serializable {
	
	private static final long serialVersionUID = 1024L;
	private String producator;
	private String model;
	private int nr_optim_echipamente;
	

	public Devices() {
		this.producator = new String();
		this.model = new String();
		this.nr_optim_echipamente = 0;
	}

	public String getProducator() {
		return producator;
	}

	public void setProducator(String producator) {
		this.producator = producator;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public int getNr_optim_echipamente() {
		return nr_optim_echipamente;
	}

	public void setNr_optim_echipamente(int nr_optim_echipamente) {
		this.nr_optim_echipamente = nr_optim_echipamente;
	}

	public Devices(String producator, String model, int nr_optim_echipamente) {
		this.producator = producator;
		this.model = model;
		this.nr_optim_echipamente = nr_optim_echipamente;
	}
	
}
