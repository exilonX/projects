import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import aprovizionare.Devices;
import aprovizionare.Offer;
import aprovizionare.Aprovizionare;


public class ClientAprovizionare {
	
	
	public static void main(String[] args) {        
		if (System.getSecurityManager() == null) {
			System.setSecurityManager(new SecurityManager());
		}

		try {
			String serviceName = "AprovizionareService";
			Registry registry = LocateRegistry.getRegistry(args[0]);
			Aprovizionare aprovizionare = (Aprovizionare)registry.lookup(serviceName);
			Devices[] device = new Devices[3];
			device[0] = new Devices("intel", "I3", 3);
			device[1] = new Devices("intel", "I5", 2);
			device[2] = new Devices("intel", "I7", 3);
			int id = aprovizionare.setProjectInquiry("numeproiect1", "descriere proiect" , device);
			System.out.println("Astept sa completeze coordonatorul de proiect");
			while (aprovizionare.getProjectInquiryStatus(id) == 0) {
				// asteapta confirmarea de la coordonatorul de proiect
			}
			
			Offer offer = aprovizionare.getProjectInquiry(id);
			if (offer == null ) {
				System.out.println("oferta nula");
			} else {
				System.out.println(offer.getTermen_limita());
				System.out.println(offer.getCost_estimativ());
			}
			aprovizionare.setProjectRequest(id);
			
			int id1 = aprovizionare.setProjectInquiry("numeproiect2", "descriere proiect2" , device);
			System.out.println("Astept sa completeze coordonatorul de proiect");
			System.out.println(id1);
			while (aprovizionare.getProjectInquiryStatus(id1) == 0) {
				// asteapta confirmarea de la coordonatorul de proiect
			}
			
			offer = aprovizionare.getProjectInquiry(id1);
			if (offer == null ) {
				System.out.println("oferta nula");
			} else {
				System.out.println("Termen Limita : " + offer.getTermen_limita());
				System.out.println("Cost Estimativ : " + offer.getCost_estimativ());
			}
			aprovizionare.setProjectRequest(id1);
			
			
		} catch (Exception exception) {
			System.out.println("exceptie: "+ exception.getMessage());
		}
	}

}
