Drop DATABASE Grupa341C4_MercaIonel_ProdIntel;
CREATE DATABASE Grupa341C4_MercaIonel_ProdIntel;

USE Grupa341C4_MercaIonel_ProdIntel;


CREATE table modele (
	
	id_model 	INT(2) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
	model 		VARCHAR(20) NOT NULL,
	stoc		INT(5) UNSIGNED NOT NULL,
	pret		INT(5) UNSIGNED NOT NULL,
	timp_livrare	INT(2) UNSIGNED NOT NULL
	);

insert into modele values (default, "gx456", 100, 1000,3);
insert into modele values (default, "gx999", 200, 500,4);
insert into modele values (default, "kp2831", 100, 100,1);
insert into modele values (default, "kp831", 100, 200,1);
insert into modele values (default, "xf3432", 100, 60,2);
insert into modele values (default, "xf345", 10, 1500,2);

insert into modele values (default,"core2", 100, 300,1);
insert into modele values (default,"pentium", 100, 300,3);
insert into modele values (default,"I3", 50, 600,2);
insert into modele values (default,"I5", 70, 640,5);
insert into modele values (default,"I7", 100, 1000,5);
insert into modele values (default,"quad", 30, 700,2);

