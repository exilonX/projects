Drop database Grupa341C4_MercaIonel_Aprovizionare;

CREATE DATABASE Grupa341C4_MercaIonel_Aprovizionare;

USE Grupa341C4_MercaIonel_Aprovizionare;

CREATE TABLE departament (
	id_departament	INT(2) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
	denumire	enum('programare', 'Q&A', 'contabilitate', 'resurse umane') NOT NULL
	#resp_dep BIGINT(13) UNSIGNED NOT NULL,
    #FOREIGN KEY (resp_dep) REFERENCES utilizatori (CNP) ON UPDATE CASCADE ON DELETE CASCADE
);

ALTER TABLE departament ADD CONSTRAINT denumire_chk CHECK (denumire IN ('programare', 'Q&A',
 'contabilitate', 'resurse umane'));

insert into departament (id_departament, denumire) 
    values (default, 'programare');

insert into departament (id_departament, denumire) 
    values (default, 'resurse umane');

insert into departament (id_departament, denumire) 
    values (default, 'Q&A');

insert into departament (id_departament, denumire) 
    values (default, 'contabilitate');
insert into departament (id_departament, denumire)
    values (default, 'admin');

CREATE TABLE utilizatori (
	CNP			BIGINT(13) UNSIGNED PRIMARY KEY NOT NULL,
	nume			VARCHAR(30) NOT NULL,
	prenume			VARCHAR(30) NOT NULL,
	adresa			VARCHAR(100) NOT NULL,
	telefon			INT(10),
	email			VARCHAR(60) NOT NULL,
    	IBAN           		VARCHAR(30) NOT NULL,
    	nr_contract     	INT(10) NOT NULL,
   	 data_angajarii  	DATETIME NOT NULL,
    	functia         	VARCHAR(60) NOT NULL,
    	nume_utilizator 	VARCHAR(20) NOT NULL,
    	parola          	VARCHAR(20) NOT NULL,
	tip		        enum('admin', 'super_admin', 'angajat') NOT NULL DEFAULT 'angajat',
    	salariu_n       	INT(6) NOT NULL,
    	id_departament  	INT(2) UNSIGNED NOT NULL,
	ore_contract		INT(5),
    	FOREIGN KEY (id_departament) REFERENCES departament (id_departament) ON UPDATE CASCADE ON DELETE CASCADE
);

ALTER TABLE utilizatori ADD CONSTRAINT email_chk CHECK (email LIKE '%@%.%');
ALTER TABLE utilizatori ADD CONSTRAINT tip_chk CHECK (tip IN ('admin', 'super_admin', 'angajat'));


INSERT INTO utilizatori (CNP, nume, prenume, adresa, telefon, email, IBAN, 
    nr_contract, data_angajarii, functia, nume_utilizator, 
    parola, tip, salariu_n, id_departament, ore_contract)
 VALUES('1320713372318', 'MARICA', 'Ionel', '-', '0', 'marica.ionel@google.com', 
'RO135AW342353312123', '2312', '1997-3-11', 'admin', 'marica.ionel', 'a',
 'admin',  '2300', '1', '160');



INSERT INTO utilizatori (CNP, nume, prenume, adresa, telefon, email, IBAN, 
    nr_contract, data_angajarii, functia, nume_utilizator, 
    parola, tip, salariu_n, id_departament, ore_contract)
 VALUES('1700713372323', 'NEAGU', 'Vasile', '-', '0', 'neagu.vasile@google.com', 
'RO135AW3421314124124', '2222', '1999-5-11', 'super-admin', 'neagu.vasile', 'a',
 'super_admin',  '2300', '1', '160');
INSERT INTO utilizatori (CNP, nume, prenume, adresa, telefon, email, IBAN, 
    nr_contract, data_angajarii, functia, nume_utilizator, 
    parola, tip, salariu_n, id_departament, ore_contract)
 VALUES('1220713379378', 'ZOTA', 'Daniel', '-', '0', 'zota.daniel@google.com', 
'RO135AWJQI1H4J12H41J', '231', '1992-3-11', 'programator', 'zota.daniel', 'a',
 'angajat',  '2300', '1', '160');



INSERT INTO utilizatori (CNP, nume, prenume, adresa, telefon, email, IBAN, 
    nr_contract, data_angajarii, functia, nume_utilizator, 
    parola, tip, salariu_n, id_departament, ore_contract)
 VALUES('1160121873449', 'NEGREANU', 'Mircea', '-', '0', 'negreanu.mircea@hotmail.com',
 'RO135AWJQJEQKWEJQ', '142', '2002-4-11', 'programator', 'negreanu.mircea', 'a',
 'angajat', '2500', '1', '160');

INSERT INTO utilizatori (CNP, nume, prenume, adresa, telefon, email, IBAN, 
    nr_contract, data_angajarii, functia, nume_utilizator, 
    parola, tip, salariu_n, id_departament, ore_contract)
 VALUES('1940503835624', 'DRAGUSIN', 'Teodora', '-', '0', 'dragusin.teodora@oracle.com',
'RO135AWJ14H12J4HJ1K2', '152', '2001-4-11', 'programator', 'dragusin.teodora', 'a',
 'angajat', '2500', '1', '160');


INSERT INTO utilizatori (CNP, nume, prenume, adresa, telefon, email, IBAN, 
    nr_contract, data_angajarii, functia, nume_utilizator, 
    parola, tip, salariu_n, id_departament, ore_contract)
 VALUES('1560330668692', 'ROGOBETE', 'Mircea', '-', '0', 'rogobete.mircea@live.com',
 'RO135AWJQUIWEYQUIWEY', '144', '2004-5-1', 'programator', 'rogobete.mircea', 'a',
 'angajat', '2800', '1', '160');




INSERT INTO utilizatori (CNP, nume, prenume, adresa, telefon, email, IBAN, 
    nr_contract, data_angajarii, functia, nume_utilizator, 
    parola, tip, salariu_n, id_departament, ore_contract)
 VALUES('2981007225570', 'GIUMALE', 'Petre', '-', '0', 'giumale.petre@yahoo.com',
 'RO1352K1J42412312414', '333', '2005-10-1', 'resurse umane', 'giumale.petre', 'a',
 'angajat', '2800', '3', '160');

INSERT INTO utilizatori (CNP, nume, prenume, adresa, telefon, email, IBAN, 
    nr_contract, data_angajarii, functia, nume_utilizator, 
    parola, tip, salariu_n, id_departament, ore_contract)
 VALUES('1350615180258', 'DRAGUSIN', 'Camelia', '-', '0', 'dragusin.camelia@live.com',
 'RO1352K1J44H12HJK23', '433', '2005-10-1', 'resurse umane', 'dragusin.camelia', 'a',
 'angajat', '1800', '3', '160');

INSERT INTO utilizatori (CNP, nume, prenume, adresa, telefon, email, IBAN, 
    nr_contract, data_angajarii, functia, nume_utilizator, 
    parola, tip, salariu_n, id_departament, ore_contract)
 VALUES('1860926240323', 'IONESCU', 'Tudor', '-', '0', 'ionescu.tudor@google.com', 
 'RO1352K1J412KJ3L123', '341', '2010-09-1', 'qa', 'ionescu.tudor', 'a',
 'angajat', '2000', '3', '160');

INSERT INTO utilizatori (CNP, nume, prenume, adresa, telefon, email, IBAN, 
    nr_contract, data_angajarii, functia, nume_utilizator, 
    parola, tip, salariu_n, id_departament, ore_contract)
 VALUES('1921202602499', 'GEORGESCU', 'Monica', '-', '0', 'georgescu.monica@yahoo.com',
  'RO1352K1J412KJ3L123', '341', '2010-09-1', 'qa', 'georgescu.monica', 'a',
 'angajat', '3800', '3', '160');



INSERT INTO utilizatori (CNP, nume, prenume, adresa, telefon, email, IBAN, 
    nr_contract, data_angajarii, functia, nume_utilizator, 
    parola, tip, salariu_n, id_departament, ore_contract)
 VALUES('1320810148322', 'POPA', 'Monica', '-', '0', 'popa.monica@yahoo.com',
 'RO1352K1J4121451512', '371', '2012-09-1', 'qa', 'popa.monica', 'a',
 'angajat', '1800', '3','160');

INSERT INTO utilizatori (CNP, nume, prenume, adresa, telefon, email, IBAN, 
    nr_contract, data_angajarii, functia, nume_utilizator, 
    parola, tip, salariu_n, id_departament, ore_contract)
 VALUES('2390831245242', 'CHIVU', 'Camelia', '-', '0', 'chivu.camelia@yahoo.com',
 'RO1352K1J4121251DAKJ2', '351', '2012-09-1', 'qa', 'chivu.camelia', 'a',
 'angajat', '1100', '3', '160');



INSERT INTO utilizatori (CNP, nume, prenume, adresa, telefon, email, IBAN, 
    nr_contract, data_angajarii, functia, nume_utilizator, 
    parola, tip, salariu_n, id_departament, ore_contract)
 VALUES('1400515716087', 'POPA', 'Cristina', '-', '0', 'popa.cristina@yahoo.com',
 'RO1352K1J4JK14124J12K4', '451', '2013-01-11', 'contabilitate', 'popa.cristina', 'a',
 'angajat', '1100', '1', '160');


INSERT INTO utilizatori (CNP, nume, prenume, adresa, telefon, email, IBAN, 
    nr_contract, data_angajarii, functia, nume_utilizator, 
    parola, tip, salariu_n, id_departament, ore_contract)
 VALUES('2740606757054', 'VASILESCU', 'Radu', '-', '0', 'vasilescu.radu@google.com',
  'RO1352K1J4JK21213J4H2', '411', '2000-01-11', 'contabilitate', 'vasilescu.radu', 'a',
 'angajat', '4100', '3', '160');

INSERT INTO utilizatori (CNP, nume, prenume, adresa, telefon, email, IBAN, 
    nr_contract, data_angajarii, functia, nume_utilizator, 
    parola, tip, salariu_n, id_departament, ore_contract)
 VALUES('1650606757054', 'Radu', 'Ion', '-', '0', 'vasilescu.radu@google.com',
  'RO1352K1J4JK21213J4H2', '411', '2000-01-11', 'coordonator', 'radu.ion', 'a',
 'angajat', '4100', '3', '160');


CREATE table asoc_responsabil_departament (
    id_asoc             INT(2) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
    id_utilizator       BIGINT(13) UNSIGNED NOT NULL,
    id_departament      INT(2) UNSIGNED NOT NULL,
    FOREIGN KEY (id_utilizator) REFERENCES utilizatori (CNP) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (id_departament) REFERENCES departament (id_departament) ON UPDATE CASCADE ON DELETE CASCADE
);

insert into asoc_responsabil_departament (id_asoc, id_utilizator, id_departament) 
    values (default, '1220713379378', 1);

insert into asoc_responsabil_departament (id_asoc, id_utilizator, id_departament) 
    values (default, '1320810148322', 3);






CREATE TABLE echipa (
	id_echipa	INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
	denumire	VARCHAR(30) NOT NULL,
    id_resp     BIGINT(13) UNSIGNED NOT NULL,
    id_dept     INT(2) UNSIGNED NOT NULL,
    FOREIGN KEY (id_resp) REFERENCES utilizatori (CNP) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (id_dept) REFERENCES departament (id_departament) ON UPDATE CASCADE ON DELETE CASCADE
);

insert into echipa (id_echipa, denumire, id_resp, id_dept) 
    values (default, 'programare1', '1220713379378', '1');
insert into echipa (id_echipa, denumire, id_resp, id_dept) 
    values (default, 'programare2', '1160121873449', '1');
insert into echipa (id_echipa, denumire, id_resp, id_dept) 
    values (default, 'qa1', '1860926240323', '3');
insert into echipa (id_echipa, denumire, id_resp, id_dept) 
    values (default, 'qa2', '1921202602499', '3');



CREATE TABLE asoc_echipa_utiliz (
    id_asoc     INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
	id_echipa	INT(10) UNSIGNED NOT NULL,
    id_utiliz   BIGINT(13) UNSIGNED NOT NULL,
    data_start  DATE NOT NULL,
    data_final  DATE NOT NULL,
    FOREIGN KEY (id_echipa) REFERENCES echipa (id_echipa) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (id_utiliz) REFERENCES utilizatori (CNP) ON UPDATE CASCADE ON DELETE CASCADE
);

insert into asoc_echipa_utiliz (id_asoc, id_echipa, id_utiliz, data_start, data_final) 
    values (default, '1', '1560330668692', '2013-01-1', '2013-09-1');

insert into asoc_echipa_utiliz (id_asoc, id_echipa, id_utiliz,data_start, data_final) 
    values (default, '2', '1940503835624', '2013-04-1', '2013-06-1');

insert into asoc_echipa_utiliz (id_asoc, id_echipa, id_utiliz, data_start, data_final) 
    values (default, '3', '2390831245242', '2013-10-1', '2013-12-1');

insert into asoc_echipa_utiliz (id_asoc, id_echipa, id_utiliz, data_start, data_final) 
    values (default, '4', '1320810148322', '2013-10-1', '2014-01-1');

insert into asoc_echipa_utiliz (id_asoc, id_echipa, id_utiliz, data_start, data_final) 
    values (default, '1', '1220713379378', '2013-01-1', '2013-09-1');

insert into asoc_echipa_utiliz (id_asoc, id_echipa, id_utiliz, data_start, data_final) 
    values (default, '2', '1160121873449', '2013-04-1', '2013-06-1');

insert into asoc_echipa_utiliz (id_asoc, id_echipa, id_utiliz, data_start, data_final) 
    values (default, '3', '1860926240323', '2013-10-1', '2013-12-1');

insert into asoc_echipa_utiliz (id_asoc, id_echipa, id_utiliz, data_start, data_final) 
    values (default, '4',  '1921202602499', '2013-10-1', '2014-01-1');

CREATE TABLE activitate (
	id_intrare	INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
    id_utiliz   BIGINT(13) UNSIGNED NOT NULL,
    data        DATE NOT NULL,
    ora_sosire  TIME,
    ora_plecare TIME,
    concediu    enum('NONE', 'odihna', 'medical', 'neplatit', 'special') NOT NULL DEFAULT 'NONE',
    FOREIGN KEY (id_utiliz) REFERENCES utilizatori (CNP) ON UPDATE CASCADE ON DELETE CASCADE
);

delimiter //
CREATE TRIGGER chk_concediu
BEFORE INSERT ON activitate
FOR EACH ROW
BEGIN
    declare msg varchar(100);
    IF ((NEW.ora_sosire = null and  NEW.ora_plecare = null) and NEW.concediu = 'NONE') THEN
        set NEW.concediu = 'special';
  END IF;
END; //
delimiter ;


CREATE TABLE facturi (
    id_factura      INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
    data_emitere    DATE NOT NULL,
    numar_factura   VARCHAR(10) NOT NULL,
    suma            INT(10) UNSIGNED
);




CREATE TABLE proiect (
	id_proiect	INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
    denumire	VARCHAR(100) NOT NULL,
    descriere   VARCHAR(100) NOT NULL
);

insert into proiect (id_proiect, denumire, descriere) 
    values (default, 'proiect1', 'acest este un proiect erp 1');
insert into proiect (id_proiect, denumire, descriere) 
    values (default, 'proiect2', 'acest este un proiect erp 2');


CREATE TABLE asoc_factura_proiect (
	id_asoc_fp	INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
	id_factura  INT(10) UNSIGNED NOT NULL,
    id_proiect  INT(10) UNSIGNED NOT NULL,
    tip    enum('in', 'out') NOT NULL DEFAULT 'in',
    FOREIGN KEY (id_factura) REFERENCES facturi (id_factura) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (id_proiect) REFERENCES proiect (id_proiect) ON UPDATE CASCADE ON DELETE CASCADE
);



create table asociere_proiect_utilizator (
	id_asoc             INT(4) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
	id_utilizator       BIGINT(13) UNSIGNED NOT NULL,
	id_proiect	INT(10) UNSIGNED NOT NULL,
    data_start  DATE NOT NULL,
    data_final  DATE NOT NULL,
	FOREIGN KEY (id_proiect) REFERENCES proiect (id_proiect) ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (id_utilizator) REFERENCES utilizatori (CNP) ON UPDATE CASCADE ON DELETE CASCADE
	);

insert into asociere_proiect_utilizator 
	values (default, '1700713372323', '1', '2013-11-12', '2014-02-01');
insert into asociere_proiect_utilizator 
	values (default, '1320713372318', '1', '2013-11-12', '2014-02-01');
insert into asociere_proiect_utilizator  
	values (default, '1220713379378', '2', '2013-12-12', '2014-05-01');



create table salarii (
    id_intrare          INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
    id_utilizator       BIGINT(13) UNSIGNED NOT NULL,
    luna               	DATE,
    suma                INT(10) UNSIGNED NOT NULL,
    FOREIGN KEY (id_utilizator) REFERENCES utilizatori (CNP) ON UPDATE CASCADE ON DELETE CASCADE
);


insert into salarii (id_intrare, id_utilizator, luna, suma) values (default, '1700713372323', '2012-08-01', '2300');
insert into salarii (id_intrare, id_utilizator, luna, suma) values (default, '1320713372318', '2012-08-01', '2300');

insert into salarii (id_intrare, id_utilizator, luna, suma) values (default, '1220713379378', '2012-08-01', '2300');
insert into salarii (id_intrare, id_utilizator, luna, suma) values (default, '1160121873449', '2012-08-01', '2500');

insert into salarii (id_intrare, id_utilizator, luna, suma) values (default, '1940503835624', '2012-08-01', '2500');
insert into salarii (id_intrare, id_utilizator, luna, suma) values (default, '1560330668692', '2012-08-01', '2800');

insert into salarii (id_intrare, id_utilizator, luna, suma) values (default, '2981007225570', '2012-08-01', '2800');
insert into salarii (id_intrare, id_utilizator, luna, suma) values (default, '1350615180258', '2012-08-01', '1800');
insert into salarii (id_intrare, id_utilizator, luna, suma) values (default, '1860926240323', '2012-08-01', '2000');
insert into salarii (id_intrare, id_utilizator, luna, suma) values (default, '1921202602499', '2012-08-01', '3800');

insert into salarii (id_intrare, id_utilizator, luna, suma) values (default, '1320810148322', '2012-08-01', '1800');
insert into salarii (id_intrare, id_utilizator, luna, suma) values (default, '2390831245242', '2012-08-01', '1100');
insert into salarii (id_intrare, id_utilizator, luna, suma) values (default, '1400515716087', '2012-08-01', '1100');
insert into salarii (id_intrare, id_utilizator, luna, suma) values (default, '2740606757054', '2012-08-01', '4100');

create table device (
	id_device			INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
	model				VARCHAR(20) NOT NULL,
	producator			VARCHAR(20) NOT NULL
);

insert into device (model, producator) values ("p43neof", "asus"); #1
insert into device (model, producator) values ("p43neo", "asus"); #2 
insert into device (model, producator) values ("alfax2", "asus"); #3
insert into device (model, producator) values ("G7123", "asus"); #4
insert into device (model, producator) values ("J2312", "asus"); #5

insert into device (model, producator) values ("core2", "intel"); #6
insert into device (model, producator) values ("I3", "intel"); #7
insert into device (model, producator) values ("I5", "intel"); #8
insert into device (model, producator) values ("I7", "intel");  #9
insert into device (model, producator) values ("quad", "intel"); #10
insert into device (model, producator) values ("gx456", "intel");
insert into device (model, producator) values ("gx999", "intel");
insert into device (model, producator) values ("kp2831", "intel");
insert into device (model, producator) values ("kp831", "intel");
insert into device (model, producator) values ("xf3432","intel");
insert into device (model, producator) values ("xf345", "intel");
insert into device (model, producator) values ("pentium", "intel");

insert into device (model, producator) values ("G2222", "asus");

insert into device (model, producator) values ("9400gt", "asus");
insert into device (model, producator) values ("9600gt", "asus");
insert into device (model, producator) values ("9800GT", "asus");
insert into device (model, producator) values ("9800gtx", "asus");
insert into device (model, producator) values ("GTX260", "asus");
insert into device (model, producator) values ("gtx240", "asus");



create table cerere (
	id_cerere			INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
	nume_proiect		VARCHAR(20) NOT NULL,
	descriere_proiect	VARCHAR(40) NOT NULL,
	status      		enum('analizata', 'neanalizata') default 'neanalizata',
	nr_programatori		INT(10) UNSIGNED NOT NULL,
	nr_qa				INT(10) UNSIGNED NOT NULL,
	nr_ore				INT(10) UNSIGNED NOT NULL,
	complexitate		INT(3) UNSIGNED NOT NULL
	);
	
create table asoc_cerere_device (
	id_asociere			INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
	id_cerere			INT(10) UNSIGNED NOT NULL,
	model				VARCHAR(20) NOT NULL,
	producator			VARCHAR(20) NOT NULL,
	numar_optim			INT(10) UNSIGNED NOT NULL,
	FOREIGN KEY (id_cerere) REFERENCES cerere (id_cerere) ON UPDATE CASCADE ON DELETE CASCADE
	);

create table echipamente (
	id_intrare			INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
	id_device			INT(10) UNSIGNED NOT NULL,
	numar				INT(10) UNSIGNED NOT NULL,
	FOREIGN KEY (id_device) REFERENCES device (id_device) ON UPDATE CASCADE ON DELETE CASCADE
	);

insert into echipamente (id_device, numar) values ('1', 10);
insert into echipamente (id_device, numar) values ('2', 10);
insert into echipamente (id_device, numar) values ('3', 10);
insert into echipamente (id_device, numar) values ('4', 10);
insert into echipamente (id_device, numar) values ('5', 10);
insert into echipamente (id_device, numar) values ('6', 10);
insert into echipamente (id_device, numar) values ('7', 10);
insert into echipamente (id_device, numar) values ('8', 10);
insert into echipamente (id_device, numar) values ('9', 1);
insert into echipamente (id_device, numar) values ('10', 10);


