package GUI;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.ArrayList;

import databaseconnection.DataBaseConnection;
import entities.Activitate;
import entities.Concediu;
import entities.Entity;
import entities.Proiect;
import entities.Utilizator;

import general.Constants;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public abstract class GUI {
	private Stage                   applicationStage;
	private Scene                   applicationScene;

	private MenuBar                 applicationMenu;    
	public  VBox                    container;    
	private double                  sceneWidth, sceneHeight;
	private String 					nume_utilizator;
	private String                  tableName;
	
	public GUI() {
		container =  new VBox();
		container.setSpacing(Constants.DEFAULT_SPACING);
		container.setPadding(new Insets(Constants.DEFAULT_SPACING, Constants.DEFAULT_SPACING, 
				Constants.DEFAULT_SPACING, Constants.DEFAULT_SPACING));
	}
	
	protected void start(String[][] structure) throws Exception {
		applicationStage = new Stage();
		applicationStage.setTitle(Constants.APPLICATION_NAME);
		applicationStage.getIcons().add(new Image(Constants.ICON_FILE_NAME));
		
		Dimension screenDimension = Toolkit.getDefaultToolkit().getScreenSize();
		sceneWidth  = Constants.SCENE_WIDTH_SCALE*screenDimension.width;
		sceneHeight = Constants.SCENE_HEITH_SCALE*screenDimension.height;
		applicationScene = new Scene(new VBox(), sceneWidth, sceneHeight);
		applicationScene.setFill(Color.AZURE);

		// seteaza numele meniului in functie de structura primita
		applicationMenu = new MenuBar();
		for (int cI = 0; cI < structure.length; cI++) {
			Menu menu = new Menu(Constants.GUIHR_STRUCTURE[cI][0]);
			MenuItem menuitem = new MenuItem(Constants.GUIHR_STRUCTURE[cI][0]);
			menuitem.addEventHandler(EventType.ROOT, (EventHandler<Event>)this);
			menu.getItems().add(menuitem);
			applicationMenu.getMenus().add(menu);
		}
		((VBox)applicationScene.getRoot()).getChildren().addAll(applicationMenu);
		applicationStage.setScene(applicationScene);
		applicationStage.show();
	}
	
	protected Entity getCurrentEntity(ArrayList<Object> values) {
		switch(this.getTableName()) {
		case "utilizatori" :
			return new Utilizator(values);
		case "activitate" :
			return new Activitate(values);
		case "concediu" :
			return new Concediu(values);
		case "proiect" :
			return new Proiect(values);
		}
		return null;
	}
	
	public void populateTableView(TableView<Entity> tableContent, String whereClause) {
		try {
			ArrayList<ArrayList<Object>> values = DataBaseConnection.getTableContent(tableName, null, 
					(whereClause==null || whereClause.isEmpty())?null:whereClause, null, null);
			
			ObservableList<Entity> data = FXCollections.observableArrayList();
			
			for (ArrayList<Object> record:values) {
				data.add(getCurrentEntity(record));
			}
			
			tableContent.setItems(data);
		} catch (Exception exception) {
			System.out.println ("exceptie: "+exception.getMessage());
			exception.printStackTrace();
		}
	}
	
	public void add_table_utilizatori(ArrayList<String> attributes, TableView<Entity> tableContent) {
		// parcurgem toate atributele tabelei curente
		for (int currentIndex = 0; currentIndex < attributes.size(); currentIndex++) {
			
			TableColumn<Entity, String> column = 
					new TableColumn<Entity, String>(attributes.get(currentIndex));
			column.setMinWidth((int)(sceneWidth / attributes.size()));
			column.setCellValueFactory(
					new PropertyValueFactory<Entity,String>(attributes.get(currentIndex))
					);
			boolean res = tableContent.getColumns().addAll(column);
			if (res != true) {
				return;
			}
		}
	}
	

	public Stage getApplicationStage() {
		return applicationStage;
	}

	public void setApplicationStage(Stage applicationStage) {
		this.applicationStage = applicationStage;
	}

	public Scene getApplicationScene() {
		return applicationScene;
	}

	public void setApplicationScene(Scene applicationScene) {
		this.applicationScene = applicationScene;
	}

	public MenuBar getApplicationMenu() {
		return applicationMenu;
	}

	public void setApplicationMenu(MenuBar applicationMenu) {
		this.applicationMenu = applicationMenu;
	}

	public VBox getContainer() {
		return container;
	}

	public void setContainer(VBox container) {
		this.container = container;
	}

	public double getSceneWidth() {
		return sceneWidth;
	}

	public void setSceneWidth(double sceneWidth) {
		this.sceneWidth = sceneWidth;
	}

	public double getSceneHeight() {
		return sceneHeight;
	}

	public void setSceneHeight(double sceneHeight) {
		this.sceneHeight = sceneHeight;
	}

	public String getNume_utilizator() {
		return nume_utilizator;
	}

	public void setNume_utilizator(String nume_utilizator) {
		this.nume_utilizator = nume_utilizator;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	
}
