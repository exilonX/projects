package GUI;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.sql.SQLException;
import java.util.ArrayList;

import databaseconnection.DataBaseConnection;

import entities.Entity;
import entities.Utilizator;
import general.Constants;
import general.ReferrencedTable;
import general.Utilities;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class GUIAdmin implements EventHandler {

	private Stage				applicationStage;
	private Scene				applicationScene;
	
	private TableView<Entity>	tableUtilizatori;
	private VBox 				container;
	
	private double				sceneWidth;
	private double				sceneHeight;
	
	private ArrayList<Label>        attributeLabels;
	private ArrayList<Control>      attributeControls; 
	
	private String	tablename = "utilizatori";
	GridPane grid;
	private String 				type;
	
	public GUIAdmin() {
		container =  new VBox();
		container.setSpacing(Constants.DEFAULT_SPACING);
		container.setPadding(new Insets(Constants.DEFAULT_SPACING, Constants.DEFAULT_SPACING, 
				Constants.DEFAULT_SPACING, Constants.DEFAULT_SPACING));
	}

	@Override
	public void handle(Event arg0) {
		// TODO Auto-generated method stub
		
	}
	
	public void start(String type) throws Exception {
		this.type = type;
		applicationStage = new Stage();
		applicationStage.setTitle(Constants.APPLICATION_NAME);
		applicationStage.getIcons().add(new Image(Constants.ICON_FILE_NAME));
		
		Dimension screenDimension = Toolkit.getDefaultToolkit().getScreenSize();
		sceneWidth  = Constants.SCENE_WIDTH_SCALE*screenDimension.width;
		sceneHeight = Constants.SCENE_HEITH_SCALE*screenDimension.height;
		applicationScene = new Scene(new VBox(), sceneWidth, sceneHeight);
		applicationScene.setFill(Color.AZURE);
		// seteaza numele meniului in functie de structura primita
		
		applicationStage.setScene(applicationScene);
		applicationStage.show();
		setContent();
	}
	
	public void populateTableView(String whereClause) {
		try {
			ArrayList<ArrayList<Object>> values = DataBaseConnection.getTableContent(tablename, null, 
					(whereClause==null || whereClause.isEmpty())?null:whereClause, null, null);
			
			ObservableList<Entity> data = FXCollections.observableArrayList();
			
			for (ArrayList<Object> record:values) {
				data.add(new Utilizator(record));
			}
			
			tableUtilizatori.setItems(data);
		} catch (Exception exception) {
			System.out.println ("exceptie: "+exception.getMessage());
			exception.printStackTrace();
		}
	}
	
	private void setContent() throws SQLException {
		
		tableUtilizatori	= new TableView<>();
		container.getChildren().clear();
		tableUtilizatori.setEditable(true);
		attributeControls = new ArrayList<>();
		attributeLabels = new ArrayList<>();
		ArrayList<String> attributes = DataBaseConnection.getTableAttributes(tablename);
		

		
		add_table_utilizatori(attributes);
		add_mouse_click_table();
		
		
		container.getChildren().addAll(tableUtilizatori);
		add_labels(attributes);
		link_labels_grid();
		
		
		
		((VBox)applicationScene.getRoot()).getChildren().addAll(container);
		applicationStage.setScene(applicationScene);
		applicationStage.show();
	}
	
	public void add_labels(ArrayList<String> attributes) throws SQLException {
		final ArrayList<ReferrencedTable> referrencedTables = DataBaseConnection.getReferrencedTables(tablename);
		
		for (int currentIndex = 0; currentIndex < attributes.size(); currentIndex++) {
			
			TableColumn<Entity, String> column = new TableColumn<Entity, String>(attributes.get(currentIndex));
			column.setMinWidth((int)(sceneWidth / attributes.size()));
			column.setCellValueFactory(new PropertyValueFactory<Entity,String>(attributes.get(currentIndex)));
			
			Label attributeLabel = new Label(attributes.get(currentIndex));
			attributeLabels.add(attributeLabel);
			String foreignKeyParentTable = DataBaseConnection.foreignKeyParentTable(attributes.get(currentIndex), referrencedTables);
			if (foreignKeyParentTable != null) {
				ComboBox attributeComboBox = new ComboBox();
				try {
					ArrayList<ArrayList<Object>> tableDescription = DataBaseConnection.getTableContent(foreignKeyParentTable, DataBaseConnection.getTableDescription(foreignKeyParentTable), null, null, null);
					for (ArrayList<Object> entityDescription:tableDescription) {
						attributeComboBox.getItems().addAll(entityDescription.get(0).toString());
					}
				} catch (Exception exception) {
					System.out.println ("exceptie: "+exception.getMessage());
				}
				attributeComboBox.setMinWidth(Constants.DEFAULT_COMBOBOX_WIDTH);
				attributeComboBox.setMaxWidth(Constants.DEFAULT_COMBOBOX_WIDTH);
				attributeControls.add(attributeComboBox);
			} else {
				TextField attributeTextField = new TextField();
				attributeTextField.setPromptText(attributes.get(currentIndex));
				attributeTextField.setPrefColumnCount(Constants.DEFAULT_TEXTFIELD_WIDTH);
				attributeControls.add(attributeTextField);
			}
		}
	}
	
	public void link_labels_grid() {
		grid = new GridPane();
		grid.setPadding(new Insets(Constants.DEFAULT_SPACING, Constants.DEFAULT_SPACING, Constants.DEFAULT_SPACING, Constants.DEFAULT_SPACING));
		grid.setVgap(Constants.DEFAULT_GAP);
		grid.setHgap(Constants.DEFAULT_GAP);        

		int currentGridRow = 0, currentGridColumn = 2;

		// TO DO: exercise 2
		int i = 0;
		for (Label l : attributeLabels) {
			Control c = attributeControls.get(i);
			grid.setConstraints(l, 0, i);
			grid.getChildren().add(l);
			grid.setConstraints(c, 1, i);
			grid.getChildren().add(c);
			i++;
		}
		
		add_button_add(currentGridColumn, currentGridRow);
		currentGridRow++;
		add_button_update(currentGridColumn, currentGridRow);
		currentGridRow++;
		add_button_delete(currentGridColumn, currentGridRow);
		currentGridRow++;
		container.getChildren().addAll(grid);
	}
	
	public void add_table_utilizatori(ArrayList<String> attributes) {
		// parcurgem toate atributele tabelei curente
		for (int currentIndex = 0; currentIndex < attributes.size(); currentIndex++) {
			
			TableColumn<Entity, String> column = 
					new TableColumn<Entity, String>(attributes.get(currentIndex));
			column.setMinWidth((int)(sceneWidth / attributes.size()));
			column.setCellValueFactory(
					new PropertyValueFactory<Entity,String>(attributes.get(currentIndex))
					);
			boolean res = tableUtilizatori.getColumns().addAll(column);
			if (res != true) {
				return;
			}
		}
		if (type == Constants.SUPER_ADMIN) {
			populateTableView(null);
		}
		else {
			String whereclause = " tip = 'angajat'";
			populateTableView(whereclause);
		}
	}
	
	public void add_mouse_click_table() {
			tableUtilizatori.setOnMouseClicked(new EventHandler<MouseEvent>() { 
				@Override
				public void handle(MouseEvent event) {
					ArrayList<ReferrencedTable> referrencedTables;
					try {
						referrencedTables = DataBaseConnection.getReferrencedTables(tablename);
						ArrayList<String> values = ((Entity)tableUtilizatori.getSelectionModel().getSelectedItem()).getValues();
						int currentIndex = 0;
						for (String value:values) {
							if (attributeControls.get(currentIndex) instanceof TextField) {
								((TextField)attributeControls.get(currentIndex)).setText(value);
							} else {
								String foreignKeyParentTable = DataBaseConnection.foreignKeyParentTable(attributeLabels.get(currentIndex).getText(), referrencedTables);
								try {
									ArrayList<ArrayList<Object>> tableDescription = DataBaseConnection.getTableContent(foreignKeyParentTable, DataBaseConnection.getTableDescription(foreignKeyParentTable), DataBaseConnection.getTablePrimaryKey(foreignKeyParentTable) +"="+values.get(currentIndex), null, null);
									((ComboBox)attributeControls.get(currentIndex)).setValue(tableDescription.get(0).get(0).toString());
								} catch (Exception exception) {
									System.out.println ("exceptie: "+exception.getMessage());
								}
							}
							currentIndex++;
						}
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
			});
	}
	
	public void add_button_add(int currentGridColumn, int currentGridRow) {
		final Button addButton = new Button(Constants.ADD_BUTTON_NAME);
		addButton.setMaxWidth(Double.MAX_VALUE);
		addButton.setOnMouseClicked(new EventHandler<MouseEvent>() { 
			@Override
			public void handle(MouseEvent event) { 
				ArrayList<String> values = new ArrayList<>();
				int currentIndex = 0;
				ArrayList<ReferrencedTable> referrencedTables;
				try {
					referrencedTables = DataBaseConnection.getReferrencedTables(tablename);
					for (Control attributeControl:attributeControls) {
						if (attributeControl instanceof TextField) {
							values.add(((TextField)attributeControl).getText());
						} else {
							try {
								String foreignKeyParentTable = DataBaseConnection.foreignKeyParentTable(attributeLabels.get(currentIndex).getText(), referrencedTables);
								ArrayList<String> foreignKeyParentTablePrimaryKey = new ArrayList<>();
								foreignKeyParentTablePrimaryKey.add(DataBaseConnection.getTablePrimaryKey(foreignKeyParentTable));
								ArrayList<ArrayList<Object>> tableDescription = DataBaseConnection.getTableContent(foreignKeyParentTable, foreignKeyParentTablePrimaryKey, DataBaseConnection.getTableDescription(foreignKeyParentTable).get(0)+"=\""+((ComboBox)attributeControl).getValue()+"\"", null, null);
								values.add(tableDescription.get(0).get(0).toString());
							} catch (Exception exception) {
								System.out.println ("exceptie: "+exception.getMessage());
							}
						}
						currentIndex++;
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				try {
					DataBaseConnection.insertValuesIntoTable(tablename,null,values,false);
				} catch (Exception exception) {
					System.out.println ("exceptie: "+exception.getMessage());
				}
				populateTableView(null);
			}
		});
		GridPane.setConstraints(addButton, currentGridColumn, currentGridRow);
		grid.getChildren().add(addButton);
	}
	
	public void add_button_update(int currentGridColumn, int currentGridRow) {
		final Button updateButton = new Button(Constants.UPDATE_BUTTON_NAME);
		updateButton.setMaxWidth(Double.MAX_VALUE);
		updateButton.setOnMouseClicked(new EventHandler<MouseEvent>() { 
			@Override
			public void handle(MouseEvent event) {
				ArrayList<String> values = new ArrayList<>();
				int currentIndex = 0;
				ArrayList<ReferrencedTable> referrencedTables;
				try {
					referrencedTables = DataBaseConnection.getReferrencedTables(tablename);
					for (Control c : attributeControls) {
						if (c instanceof TextField) {
							values.add(((TextField) c).getText());

						} else {
							if (c instanceof ComboBox) {
								String foreignKeyParentTable = DataBaseConnection.foreignKeyParentTable(attributeLabels.get(currentIndex).getText(), referrencedTables);
								ArrayList<String> foreignKeyParentTablePrimaryKey = new ArrayList<>();
								foreignKeyParentTablePrimaryKey.add(DataBaseConnection.getTablePrimaryKey(foreignKeyParentTable));
								ArrayList<ArrayList<Object>> tableDescription = null;
								tableDescription = DataBaseConnection.getTableContent(foreignKeyParentTable, 
										foreignKeyParentTablePrimaryKey, DataBaseConnection.getTableDescription(foreignKeyParentTable).get(0)+"=\""+((ComboBox)c).getValue()+"\"", null, null);
								values.add(tableDescription.get(0).get(0).toString());
							}
							
						}
						currentIndex++;
					}
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				ArrayList<String> attr;
				try {
					attr = DataBaseConnection.getTableAttributes(tablename);
					try {
						DataBaseConnection.updateRecordsIntoTable(tablename, attr, values, null);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				populateTableView(null);
			}
		});
		GridPane.setConstraints(updateButton, currentGridColumn, currentGridRow);
		grid.getChildren().add(updateButton);
		
	}

	public void add_button_delete(int currentGridColumn, int currentGridRow) {
		final Button deleteButton = new Button(Constants.DELETE_BUTTON_NAME);
		deleteButton.setMaxWidth(Double.MAX_VALUE);
		deleteButton.setOnMouseClicked(new EventHandler<MouseEvent>() { 
			@Override
			public void handle(MouseEvent event) { 
				try {
					DataBaseConnection.deleteRecordsFromTable(tablename,null,null,attributeLabels.get(0).getText()+"="+((TextField)attributeControls.get(0)).getText());
				} catch (Exception exception) {
					System.out.println ("exceptie: "+exception.getMessage());
				}
				populateTableView(null);
				for (Control attributeControl:attributeControls) {
					if (attributeControl instanceof TextField) {
						((TextField)attributeControl).setText("");
					} else {
						((ComboBox)attributeControl).setValue("");
					}
				}
				try {
					((TextField)attributeControls.get(0)).setText((DataBaseConnection.getTablePrimaryKeyMaxValue(tablename)+1)+"");
				} catch (Exception exception) {
					System.out.println ("exceptie: "+exception.getMessage());
				}
			} 
		});        
		GridPane.setConstraints(deleteButton, currentGridColumn, currentGridRow++);
		grid.getChildren().add(deleteButton);
	}
}
