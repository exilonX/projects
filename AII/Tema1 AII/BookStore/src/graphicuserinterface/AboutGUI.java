package graphicuserinterface;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.ArrayList;

import org.omg.CORBA.CODESET_INCOMPATIBLE;

import dataaccess.DataBaseConnection;

import general.Constants;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class AboutGUI implements EventHandler {

    private Stage                   applicationStage;
    private Scene                   applicationScene;    
    private Label                   label;
    private Button                  button;
    
    public AboutGUI() {       
    }
    
    public void start() {
        applicationStage = new Stage();
        applicationStage.setTitle(Constants.ABOUT_TEXT);
        applicationStage.getIcons().add(new Image(Constants.ICON_FILE_NAME));
        Dimension screenDimension = Toolkit.getDefaultToolkit().getScreenSize();
        double sceneWidth  = Constants.SCENE_WIDTH_SCALE*(screenDimension.width / 2);
        double sceneHeight = Constants.SCENE_HEITH_SCALE*(screenDimension.height / 2);
        int currentGridRow = 0, currentGridColumn = 2;
        GridPane grid = new GridPane();        
       
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));
        Text scenetitle = new Text(Constants.ABOUT_TEXT);
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 12));
        grid.add(scenetitle, 0, 0, 2, 1);
        GridPane.setConstraints(scenetitle, currentGridColumn, currentGridRow++);
        final Button addButton = new Button(Constants.CANCEL_BUTTON);
        addButton.setMaxWidth(Double.MAX_VALUE);
        addButton.autosize();
        addButton.setText(Constants.CANCEL_BUTTON);
        addButton.alignmentProperty();
        addButton.setOnMouseClicked(new EventHandler<MouseEvent>() { 
          @Override
          public void handle(MouseEvent event) { 
//        	  Node  source = (Node)  event.getSource(); 
//              Stage stage  = (Stage) source.getScene().getWindow();
//              stage.close(); 
        	  applicationStage.close();
          } 
        });
        GridPane.setConstraints(addButton, currentGridColumn, currentGridRow++);
        grid.getChildren().add(addButton);
        Scene scene = new Scene(grid, 300, 275);
        applicationStage.setScene(scene);
        //applicationStage.setScene(applicationScene);
        applicationStage.show();   
    }
    
    @Override
    public void handle(Event event) {           
    	Node  source = (Node)  event.getSource(); 
        Stage stage  = (Stage) source.getScene().getWindow();
        stage.close(); 
    }     
}
