package graphicuserinterface;

import java.io.PrintWriter;
import java.util.ArrayList;

import entities.Record;
import general.Constants;
import general.Queries;

public class ClientGraphicUserInterface {

	public ClientGraphicUserInterface() { }


	public static void displayClientGraphicUserInterface(String userDisplayName, String errorMessage, String currentTableName, String cautanume, String cautadesc,
				String pretmin, String pretmax, ArrayList<Record> shoppingCart, PrintWriter printWriter) {
		String content = new String();
		content += "<html>\n";
		content += "<head>\n";
		content += "<meta http-equiv=\"content-type\" content=\"text/html; charset=ISO-8859-1\" /><title>Librarie Virtuala</title>\n";
		content += "<link rel=\"stylesheet\" type=\"text/css\" href=\"css/bookstore.css\" />\n";
		content += "</head>\n";
		content += "<body>\n";
		content += "<form name=\"formular\" action=\"ClientServlet\" method=\"POST\">\n";
		content += Constants.WELCOME_MESSAGE+ "<a href = \"EditInfoServlet\">" + userDisplayName+"</a><br/>\n";
		// TO DO (exercise 12): add control for client logout
		content += "<input type = \"submit\" value=\"Logout\" name=\"logout\">";
		content += "<center>\n";
		content += "<h2>Achizitie Produse</h2>\n";
		content += "<table  bgcolor=\"#ffffff\" border=\"0\" cellpadding=\"4\" cellspacing=\"1\"><tbody>\n";
		content += "<tr>\n";
		content += "<td align=\"left\" valign=\"top\">\n";
		content += "Cautare dupa nume" +": <input type=\"text\" name=\""+"dupanume"+"\" placeholder = \"Dupa Nume\"><br/>\n";
		content += "<input type=\"submit\" name=\""+"cautanume"+"\" value=\""+"Cauta"+"\"/><br/>\n";
		content += "Cautare dupa descriere" +": <input type=\"text\" name=\""+"dupadesc"+"\" placeholder = \"Dupa Descriere\"><br/>\n";
		content += "<input type=\"submit\" name=\""+"cautadesc"+"\" value=\""+"Cauta"+"\"/><br/>\n";
		content += "<p>Filtrare dupa pret </p>";
		content += "Pret minim" +": <input type=\"text\" name=\""+"pretmin"+"\" placeholder = \"Pret minim\"><br/>\n";
		content += "Pret maxim" +": <input type=\"text\" name=\""+"pretmax"+"\" placeholder = \"Pret maxim\"><br/>\n";
		content += "<input type=\"submit\" name=\""+"filtpret"+"\" value=\""+"Filtrare"+"\"/><br/>\n";

		content += "</td>\n";
		content += "<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>\n";
		content += "<td width=\"60%\" align=\"center\">\n";
		if (errorMessage != null)
			content += errorMessage + "<br/><br/>\n";

		String whereClause = new String();

		// TO DO (exercise 7): construct whereClause as to filter books by collections and domains

		ArrayList<String> attributes = new ArrayList<>();
		attributes.add("id");
		attributes.add("Denumire");
		attributes.add("Descriere");
		attributes.add("Pret");
		ArrayList<ArrayList<String>> tableContent = new ArrayList<>();
		if (cautadesc.length() != 0) {
			tableContent = Queries.get_projects_desc(cautadesc);
		} else if (cautanume.length() != 0) {
			tableContent = Queries.get_projects_like(cautanume);
		} else {
			tableContent = Queries.get_projects();
		}

		if (pretmin.length() != 0 && pretmax.length() != 0) {
			tableContent = Queries.get_projects_pret(pretmin, pretmax);
		}

		content += "<table  bgcolor=\"#ffffff\" border=\"0\" cellpadding=\"5\" cellspacing=\"1\"><tbody>\n";
		for (ArrayList<String> tableRow:tableContent) {
			content += "<tr>\n";
			content += "<td valign=\"top\"><img src=\"images/firefox.jpg\" width = \"100\" height = \"100\"/></td>\n";
			content += "<td>&nbsp</td>\n";
			content += "<td bgcolor=\"#ebebeb\">\n";
			int currentIndex = 0;
			for (String tableColumn:tableRow) {
				if (currentIndex != 0)
					content += attributes.get(currentIndex)+": "+tableColumn+"<br/>\n";
				currentIndex++;
			}
			content += Constants.COPIES+": <input type=\"text\" name=\""+Constants.COPIES.toLowerCase() + "_" + tableRow.get(0).toString() +"\" size=\"3\"/><br/>\n";
			content += "<input type=\"submit\" name=\""+Constants.ADD_BUTTON_NAME.toLowerCase()+ "_" + tableRow.get(0).toString() +"\" value=\""+Constants.ADD_BUTTON_NAME+"\"/><br/>\n";
			content += "</td>\n";
			content += "</tr><tr><td colspan=\"3\">&nbsp;</td></tr>\n";
		}
		content += "</tbody></table>\n";
		content += "</td>\n";
		content += "<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>\n";
		content += "<td align=\"left\" valign=\"top\">\n";
		content += "<table><tr><td><img src=\"images/shoppingcart.png\"/></td><td valign=\"center\">"+Constants.SHOPPING_CART+"</td></tr></table><br/>\n";
		if (shoppingCart != null && !shoppingCart.isEmpty()) {
			Double total = 0.0;
			// TO DO (exercise 9): get shopping cart content and display it
			for (Record r : shoppingCart) {
				String query = "id_proiect = '" + r.getAttribute() + "\'";
				ArrayList<ArrayList<String>> result = Queries.get_projects_where(query);
				System.err.println("Value:" + r.getValue() + " Att: " + r.getAttribute());
				ArrayList<String> value = result.get(0);
				content += r.getValue() + " x " + value.get(1) +
						"<br>(" + value.get(2) + ") <br>=" +
							Integer.parseInt(r.getValue()) * Double.parseDouble(value.get(3).toString()) + "<br>";
				total += Integer.parseInt(r.getValue()) * Double.parseDouble(value.get(3).toString());
			}
			content += "Total: " + total + "<br>";

			// TO DO (exercise 10): add controls for finalizing and canceling command
			content += "<br><input type = \"submit\" value = \"Golire cos\" name = \"golire\" >";
			content += "<input type = \"submit\" value = \"Finalizare comanda\" name = \"finalizare\" >";

		} else {
			content += Constants.EMPTY_CART+"<br/>\n";
		}
		content += "</td>\n";
		content += "</tr>\n";
		content += "</tbody></table>\n";
		content += "</form>\n";
		content += "</center>\n";
		content += "</body>\n";
		content += "</html>";
		printWriter.println(content);
	}
}
