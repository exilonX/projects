#pragma once
#ifndef RANDOMOBSTACLE_H
#define RANDOMOBSTACLE_H

#include "glut.h"
#include "Cub.h"
#include <vector>
#include <time.h>

class RandomObstacle
{
public:
	RandomObstacle(int nr_col, int nrlinii, GLfloat pexi, GLfloat peyi, char **labirint);
	~RandomObstacle(void);
	void DrawObstacles();
	void Init();
	std::vector<Cub> cuburi;
	std::vector<Vector3D> empty;
private:
	char **labirint;
	int nrcoloane, nrlinii;
	GLfloat pexi, peyi;
};

#endif