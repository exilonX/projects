
#include <time.h>
#include "Inamici.h"
#include <iostream>

Inamici::Inamici(void)
{
}

bool SS(Vector3D s1,Vector3D s2,float R1,float R2){
	float distance = sqrt(pow((s1.x - s2.x),2) + pow((s1.y - s2.y),2) + pow((s1.z -s2.z),2));
	return distance < (R1 + R2);
}

bool Inamici::CB(Vector3D aux, float R) {
	
	for (int i = 0; i < bombe.size(); i++) {
		Vector3D posbomb = bombe[i].get_coords();
		if (SS(aux, posbomb, R, R)) {
			std::cout << "E coliziune " << std::endl;
			return true;
		}
	}
	return false;
}

Inamici::Inamici(std::vector<Cub> labirint, std::vector<Cub> obstacole, std::vector<Vector3D> empty, std::vector<Bomba> bombe){
	this->labirint = labirint;
	this->obstacole = obstacole;
	this->empty = empty;
	this->bombe = bombe;
}

Inamici::~Inamici(void)
{
}

bool CubeSphere(Vector3D C1, Vector3D C2, Vector3D sphere,float R)
{
	float dist_squared = R*R;
	
	if(sphere.x < C1.x) dist_squared -= sqrt(sphere.x - C1.x);
	else if(sphere.x > C2.x) dist_squared -= sqrt(sphere.x - C2.x);
	
	if(sphere.y < C1.y) dist_squared -= sqrt(sphere.y - C1.y);
	else if(sphere.y > C2.y) dist_squared -= sqrt(sphere.y - C2.y);
	

	if(sphere.z < C1.z) dist_squared -= sqrt(sphere.z - C1.z);
	else if(sphere.z > C2.z) dist_squared -= sqrt(sphere.z - C2.z);
	return dist_squared > 0;
}

void Inamici::init_inamic()
{
	int i = rand() % empty.size();
	pos = empty[i];
	count = 0;
	alive = true;
}

bool Inamici::coliziune(Vector3D pos, float R)
{
	for(int i = 0; i < obstacole.size(); i++) {
		if (CubeSphere(obstacole[i].C1(), obstacole[i].C2(), pos, R)) {
			return true;
		}
	}

	for(int i = 0; i < labirint.size(); i++) {
		if (CubeSphere(labirint[i].C1(), labirint[i].C2(), pos, R)) {
			return true;
		}
	}
	return false;
}

Vector3D Inamici::next_pos()
{
	float move = 1.0f;
	std::vector<Vector3D> next;
	next.push_back(Vector3D(pos.x + move, pos.y, pos.z));
	next.push_back(Vector3D(pos.x - move, pos.y, pos.z));
	next.push_back(Vector3D(pos.x, pos.y, pos.z + move));
	next.push_back(Vector3D(pos.x, pos.y, pos.z - move));
	int i = 0;
	while (i < 100) {
		int n = rand() % 4;
		if (!coliziune(next[n], 0.7f) && !CB(next[n], 0.7f)) {
			return next[n];
		}
		i++;
	}

	return Vector3D(-1000, -1000, -1000);
}

void Inamici::draw_inamic()
{
	glPushMatrix();
	glTranslatef(pos.x, pos.y, pos.z);
	glutSolidSphere(0.7f,20,20);
	glPopMatrix();
}

