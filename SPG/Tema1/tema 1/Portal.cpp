#include "Portal.h"


Portal::Portal()
{
}

Portal::Portal(std::vector<Cub> obstacole)
{
	this->obstacole = obstacole;
}


Portal::~Portal(void)
{
}

void Portal::init()
{
	int n = rand() % obstacole.size();
	Cub c = obstacole[n];
	this->portal = Cub(c.trax, c.tray, c.traz, 1.0f, 0);
}

void Portal::draw_portal()
{
	glPushMatrix();
		portal.DrawCub();
	glPopMatrix();
}
