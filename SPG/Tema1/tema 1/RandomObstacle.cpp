#include "RandomObstacle.h"
#include <iostream>

RandomObstacle::RandomObstacle(int nr_col, int nrlinii, GLfloat pexi, GLfloat peyi, char **labirint)
{
	this->labirint = labirint;
	this->nrcoloane = nr_col;
	this->nrlinii = nrlinii;
	this->pexi = pexi; 
	this->peyi = peyi;
}


RandomObstacle::~RandomObstacle(void)
{
}

void RandomObstacle::Init()
{
	GLfloat pexi1 = pexi; 
	GLfloat peyi1 = peyi;
	srand (time(NULL));


	int nr = 0;
	for(int i = 1; i < nrlinii - 2; i++){	
		pexi1 = pexi;
		for(int j = 2; j < nrcoloane - 2; j++){
			// deseneaza cub daca este 0 altfel treci mai departe
			  /* generate secret number between 1 and 10: */
			// skip the position of the player and the one below him
			if ((! ((i == 1 || i == 2) && (j == 2 || j == 3)))){
				std::cout << i << "  " << j << std::endl;
				int iSecret = rand() % 10 + 1;
				if(labirint[i][j] == '0' && (iSecret % 2 == 0)){
				
					Cub c =  Cub(pexi1 + j*2 , 0, peyi1 + 2*i, 1, i* nrlinii + j);
					cuburi.push_back(c);
				
				}else {
					if ((iSecret % 2 == 1) && (labirint[i][j] == '0')) {
						empty.push_back(Vector3D(pexi1 + j*2 , 0, peyi1 + 2*i));
					}
				}
			}
		}
	}
}

void RandomObstacle::DrawObstacles()
{
	for(int i = 0; i < cuburi.size(); i++) {
		Cub c = cuburi[i];
		glPushMatrix();				
			c.DrawCub();
		glPopMatrix();
	}
}
