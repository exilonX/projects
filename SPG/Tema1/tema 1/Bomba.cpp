#include "Bomba.h"
#include "Cub.h"
#include <iostream>


Bomba::Bomba(GLfloat trax,GLfloat tray,GLfloat traz,float size)
{
	this->trax = trax;
	this->tray = tray;
	this->traz = traz;
	this->size = size;
	this->time = 10;
	this->charged = 0;
	this->exploded = false;
}


Bomba::~Bomba(void)
{
}

void Bomba::draw_bomba()
{
	glPushMatrix();
	glTranslatef(this->trax, this->tray, this->traz);
	charged++;
	
	float sz = charged % 10 ? 0.2f : 0.3f;
	
	glColor3f(1.0f, 1.0f, 1.0f);
	glutSolidSphere(this->size + sz,20,20);
	glPopMatrix();
}

void Bomba::DrawCub(float size, float mx, float my, float mz)
{
	glTranslatef(trax,tray,traz);
	glScalef(mx, my, mz);
		glBegin(GL_QUADS);
		glVertex3f( -1*size, -1*size, -1*size);       // P
		glVertex3f( -1*size,  1*size, -1*size);       // P2
		glVertex3f(  1*size,  1*size, -1*size);       // P3
		glVertex3f(  1*size, -1*size, -1*size);       // P4

		glVertex3f(  1*size, -1*size, 1*size );
		glVertex3f(  1*size,  1*size, 1*size );
		glVertex3f( -1*size,  1*size, 1*size );
		glVertex3f( -1*size, -1*size, 1*size );

		glVertex3f( 1*size, -1*size, -1*size );
		glVertex3f( 1*size,  1*size, -1*size );
		glVertex3f( 1*size,  1*size,  1*size );
		glVertex3f( 1*size, -1*size,  1*size );
		
		glVertex3f( -1*size, -1*size,  1*size );
		glVertex3f( -1*size,  1*size,  1*size );
		glVertex3f( -1*size,  1*size, -1*size );
		glVertex3f( -1*size, -1*size, -1*size );
	
		glVertex3f(  1*size,  1*size,  1*size );
		glVertex3f(  1*size,  1*size, -1*size );
		glVertex3f( -1*size,  1*size, -1*size );
		glVertex3f( -1*size,  1*size,  1*size );

		glVertex3f(  1*size, -1*size, -1*size );
		glVertex3f(  1*size, -1*size,  1*size );
		glVertex3f( -1*size, -1*size,  1*size );
		glVertex3f( -1*size, -1*size, -1*size );
	glEnd();
	glFlush();
}

float len = 1.0f;

void Bomba::draw_blow()
{
	charged++;
	float sz = charged % 2 ? 0.2f : 0.0f;
	if (len <= 3.0f) {
		len += sz;
	}
	glPushMatrix();
	
	DrawCub(1.0f, len,0.5f, 0.5f);
	glPopMatrix();
	glPushMatrix();
	
	DrawCub(1.0f, 0.5f, 0.5f, len);
	glPopMatrix();
}

Vector3D Bomba::get_coords()
{
	return Vector3D(trax, tray, traz);
}

void Bomba::max_coords(Vector3D *unu, Vector3D *doi)
{
	unu->x = trax + size *len;
	unu->y = tray + size *0.5;
	unu->z = traz + size *0.5;
	doi->x = trax + size *0.5;
	doi->y = tray + size *0.5;
	doi->z = traz + size *len;
}

void Bomba::min_coords(Vector3D *unu, Vector3D *doi)
{
	unu->x = trax - size *len;
	unu->y = tray - size *0.5;
	unu->z = traz - size *0.5;
	doi->x = trax - size *0.5;
	doi->y = tray - size *0.5;
	doi->z = traz - size *len;
}