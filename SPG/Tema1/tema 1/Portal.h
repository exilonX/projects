#pragma once
#ifndef PORTAL_H
#define PORTAL_H

#include "Cub.h"
#include <vector>

class Portal
{
public:
	Portal();
	Portal(std::vector<Cub> cub);
	~Portal(void);
	std::vector<Cub> obstacole;
	void draw_portal();
	void init();
	Cub portal;
};

#endif