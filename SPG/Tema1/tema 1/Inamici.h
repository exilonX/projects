#pragma once

#ifndef INAMICI_H
#define INAMICI_H

#include "Cub.h"
#include <vector>
#include "Bomba.h"

class Inamici
{
public:
	Inamici(void);
	Inamici(std::vector<Cub> labirint, std::vector<Cub> obstacole, std::vector<Vector3D> empty,std::vector<Bomba> bombe);
	~Inamici(void);
	void init_inamic();
	void draw_inamic();
	void move_inamic();
	Vector3D next_pos();
	bool CB(Vector3D aux, float R);
	bool coliziune(Vector3D pos, float R);
	Vector3D pos;
	int count;
	bool alive;
private:
	std::vector<Cub> labirint;
	std::vector<Cub> obstacole;
	std::vector<Vector3D> empty;
	std::vector<Bomba> bombe;
};

#endif
