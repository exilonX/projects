#pragma once

#ifndef BOMBA_H
#define BOMBA_H

#include "glut.h"
#include "Vector3D.h"

class Bomba
{
public:
	Bomba(GLfloat trax,GLfloat tray,GLfloat traz,float size);
	~Bomba(void);
	void draw_bomba();
	void init();
	void draw_blow();
	void DrawCub(float size, float mx, float my, float mz);
	int charged;
	Vector3D get_coords();
	void max_coords(Vector3D *unu, Vector3D *doi);
	void min_coords(Vector3D *unu, Vector3D *doi);
	bool exploded;
private:
	float size;
	GLfloat trax,tray,traz;
	int time;
};

#endif