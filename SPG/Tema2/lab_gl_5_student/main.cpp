﻿//-------------------------------------------------------------------------------------------------
// Descriere: fisier main
//
// Autor: student
// Data: today
//-------------------------------------------------------------------------------------------------

//incarcator de meshe
#include "lab_mesh_loader.hpp"
//geometrie: drawSolidCube, drawWireTeapot...
#include "lab_geometry.hpp"
//incarcator de shadere
#include "lab_shader_loader.hpp"
//interfata cu glut, ne ofera fereastra, input, context opengl
#include "lab_glut.hpp"
//texturi
#include "lab_texture_loader.hpp"
//camera
#include "camera.h"
//time
#include <ctime>


class Laborator : public lab::glut::WindowListener{

//variabile
private:

	glm::mat4 view_matrix, projection_matrix;											//matrici 4x4 pt modelare vizualizare proiectie
	unsigned int gl_program_shader;														//id-ul de opengl al obiectului de tip program shader
	unsigned int screen_width, screen_height;
	
	//grid
	glm::mat4 model_matrix_grid;
	unsigned int grid_vbo, grid_ibo, grid_vao, grid_num_indices;		

	// camera
	Camera camera_fps;
	int camera_type;
	// my position
	glm::vec3 position;
	// unghiul de rotatie
	float total3rdangle;
	// unghiul total 
	float rotate_sum, distm, rotate_angle;

	// light 
	glm::vec3 light_position;
	glm::vec3 eye_position;
	unsigned int material_shininess;
	float material_kd;
	float material_ks;

	unsigned int mesh_vbo, mesh_ibo, mesh_vao, mesh_num_indices;						//containere opengl pentru vertecsi, indecsi si stare

	unsigned int mesh_vbo_sphere, mesh_ibo_sphere, mesh_vao_sphere, mesh_num_indices_sphere;	//containere opengl pentru vertecsi, indecsi si stare pentru sfera ce va fi pusa la pozitia luminii
	
	// Vertexi si indexi pentru desenarea gridului
	lab::VertexFormat *vertex;
	unsigned int *index;
	unsigned int grid_size;
	int deformat;

public:

	Laborator(){
		//setari pentru desenare, clear color seteaza culoarea de clear pentru ecran (format R,G,B,A)
		glClearColor(0.5,0.5,0.5,1);
		glClearDepth(1);			//clear depth si depth test (nu le studiem momentan, dar avem nevoie de ele!)
		glEnable(GL_DEPTH_TEST);	//sunt folosite pentru a determina obiectele cele mai apropiate de camera (la curs: algoritmul pictorului, algoritmul zbuffer)
		
		// initializare camera
		camera_fps.init();
		deformat = 0;
		camera_type= 1;

		// initializare positia initiala
		position = glm::vec3(40.0f, 20.0f, 60.0f);
		total3rdangle = 0.0f;
		rotate_sum = 0.0f; 
		distm = 1; 
		rotate_angle = 0.1f;

		//incarca un shader din fisiere si gaseste locatiile matricilor relativ la programul creat
		gl_program_shader = lab::loadShader("shadere\\shader_vertex.glsl", "shadere\\shader_fragment.glsl");
		model_matrix_grid = glm::mat4();

		//incarca o sfera
		lab::loadObj("resurse\\sphere.obj",mesh_vao_sphere, mesh_vbo_sphere, mesh_ibo_sphere, mesh_num_indices_sphere);	

		//lumina & material
		eye_position = glm::vec3(0, 10, 50);
		light_position = glm::vec3(150,30,100);
		material_shininess = 100;
		material_kd = 0.5;
		material_ks = 0.5;
		// numarul de puncte initial pentru grid
		grid_size = 25;
		
		if (deformat != 0) {
			draw_grid_def(grid_size);
		} else {
			flat(grid_size);
		}
		
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}

	//destructor .. e apelat cand e distrusa clasa
	~Laborator(){
		//distruge shader
		glDeleteProgram(gl_program_shader);
		//distruge mesh incarcat
		glDeleteBuffers(1,&mesh_vbo);
		glDeleteBuffers(1,&mesh_ibo);
		glDeleteVertexArrays(1,&mesh_vbo);

		//distruge sfera
		glDeleteBuffers(1,&mesh_vbo_sphere);
		glDeleteBuffers(1,&mesh_ibo_sphere);
		glDeleteVertexArrays(1,&mesh_vbo_sphere);
		//distruge obiecte
		glDeleteBuffers(1,&grid_vbo);	glDeleteBuffers(1,&grid_ibo);	glDeleteVertexArrays(1,&grid_vao);
	}

	// structura ce contine varfurile unui triunghi luate in sensul acelor de
	// ceasornic A, B, C
	struct Triangle {
		glm::vec3 varfA;
		glm::vec3 varfB;
		glm::vec3 varfC;
	};

	// functie care calculeaza normala unui triunghi ca produsul vectorial 
	// dintre diferenta muchiilor
	glm::vec3 compute_normal_triangle(Triangle t) {
		//print_triangle(t);
		glm::vec3 normal = glm::normalize(glm::cross(t.varfA - t.varfC, t.varfA - t.varfB));
		return normal;
	}

	// incarca un height map din fisier
	unsigned char* get_heightmap(unsigned int *width, unsigned int *height) 
	{
		unsigned char *data= lab::_loadBMPFile("resurse\\heightmap.bmp", *width, *height);
		return data;
	}

	// functie care calculeaza media normalelor triunghiurilor adiacente 
	// unui anumit punct de pe grid
	glm::vec3 medie_normale(std::vector<Triangle> triung_adiacente)
	{
		glm::vec3 sum = glm::vec3();
		for (int i = 0; i < triung_adiacente.size(); i++) {
			sum += compute_normal_triangle(triung_adiacente[i]);
		}
		return glm::normalize(sum);
	}

	
	// conversie de la VertexFormat la glm::vec3
	glm::vec3 vformat_to_vec3(lab::VertexFormat x)
	{
		return glm::vec3(x.position_x, x.position_y, x.position_z);
	}

	// functie care returneaza o lista cu triunghiurile care sunt adiacente 
	// punctului specificat de index din vertex
	std::vector<Triangle> get_adiacent_triangles(int index, int size,int i)
	{
		int double_size = size * size;
		std::vector<Triangle> triangles;

		// primul triunghi din stang jos
		if ((index - 1 >= 0) && (index - 1 >= i * size)
			&& (index + size < double_size)) {
			Triangle t1;
			t1.varfA = vformat_to_vec3(vertex[index - 1]);
			t1.varfB = vformat_to_vec3(vertex[index]);
			t1.varfC = vformat_to_vec3(vertex[index + size]);
			triangles.push_back(t1);
		}

		// al doilea triunghi din stanga centru 
		if ((index - 1 >= 0) && (index - 1 >= i * size)  &&
			((index - size - 1) >= 0) && ((index - size - 1) >= ((i - 1) * size))) {
			Triangle t1;
			t1.varfA = vformat_to_vec3(vertex[index - 1]);
			t1.varfB = vformat_to_vec3(vertex[index - size - 1]);
			t1.varfC = vformat_to_vec3(vertex[index]);
			triangles.push_back(t1);
		}

		// triunghiul 3 stanga sus
		if (((index - size - 1) >= 0) && ((index - size - 1) >= ((i - 1) * size)) &&
				(index - size >= 0)) {
			Triangle t1;
			t1.varfA = vformat_to_vec3(vertex[index - size - 1]);
			t1.varfB = vformat_to_vec3(vertex[index - size]);
			t1.varfC = vformat_to_vec3(vertex[index]);
			triangles.push_back(t1);
		}

		// triunghiul 4 dreapta sus
		if ((index - size >= 0) && ((index + 1) < ((i + 1) * size))) {
			Triangle t1;
			t1.varfA = vformat_to_vec3(vertex[index]);
			t1.varfB = vformat_to_vec3(vertex[index - size]);
			t1.varfC = vformat_to_vec3(vertex[index + 1]);
			triangles.push_back(t1);
		}

		//triunghiul 5 dreapta centru
		if (((index + 1) < ((i + 1) * size)) && ((index + size + 1) < ((i + 2) * size)) &&
			((index + size + 1) < double_size)) {
			Triangle t1;
			t1.varfA = vformat_to_vec3(vertex[index]);
			t1.varfB = vformat_to_vec3(vertex[index + 1]);
			t1.varfC = vformat_to_vec3(vertex[index + size + 1]);
			triangles.push_back(t1);
		}

		// triunghiul 6 dreapta jos
		if (((index + size + 1) < ((i + 2) * size)) && ((index + size + 1) < double_size) &&
			((index + size) < double_size)) {
			Triangle t1;
			t1.varfA = vformat_to_vec3(vertex[index + size]);
			t1.varfB = vformat_to_vec3(vertex[index]);
			t1.varfC = vformat_to_vec3(vertex[index + size + 1]);
			triangles.push_back(t1);
		}
		
		return triangles;
	}

	//functie care calculeaza normala corespunzatoare pentru 
	// vertexul de pe pozitia index
	glm::vec3 normala_vertex(int index, int size, int i)
	{
		std::vector<Triangle> triangles = get_adiacent_triangles(index, size, i);
		return medie_normale(triangles);
	}

	//functie care initializeaza gridul deformat
	int compute_grid_def(const unsigned int size) 
	{
		
		float dim = 100.0 / size * 4;
		
		vertex = new lab::VertexFormat[size * size];
		
		const unsigned int num_ind = (size - 1) * (size - 1) * 6;
		index = new unsigned int[num_ind];
		// load the height map
		unsigned int width, height;
		unsigned char *map = get_heightmap(&width, &height);
		
		float dim_x, dim_z = 0.0f;
		int indx_v = 0;
		int indx_i = 0;
		
		unsigned int double_size = size * size;
		// generam un nr de size * size puncte si formam din aceste puncte 
		// patrate specificand indexi corespunzator
		for (int i = 0; i < size; i++) {
			dim_x = 0;
			int h_z = width * i / size;
			for (int j = 0; j < size; j++) {
				int h_x = width * j / size; 
				// am inmultit cu 3 pt bmp rgb
				unsigned char y = map[3 * (h_z * width + h_x)];

				vertex[indx_v++] = lab::VertexFormat(dim_x, y/5.0f, dim_z);
				dim_x += dim;
				//specific cei 6 indexi pentru a face patratul
				if ((i < (size - 1)) && (j < (size - 1)))
				{
					index[indx_i++] = (i + 1) * size + j;
					index[indx_i++] = i * size + j;
					index[indx_i++] = (i + 1) * size + j + 1;
					index[indx_i++] = (i + 1) * size + j + 1;
					index[indx_i++] = i * size + j;
					index[indx_i++] = i * size + j + 1;
				}
			}
			dim_z += dim;
		}
		

		// 6 triunghuri adiacente pentru fiecare vertex 
		// trebuie sa verificam daca exista acel triunghi si daca da
		// il adaugam 
		int index_n = 0;
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				int index = i * size + j;
				// calculam normala pentru vertexul curent si modificam
				// si updatam
				glm::vec3 normala = normala_vertex(index, size, i);
				vertex[index_n].normal_x = normala.x;
				vertex[index_n].normal_y = normala.y;
				vertex[index_n].normal_z = normala.z;
				++index_n;
			}
		}

		delete[] map;
		return num_ind;
	}

	// deseneaza gridul deformat
	void draw_grid_def(const unsigned int x)
	{
		unsigned int size = x;
		// genereaza gridul 
		unsigned int num_ind = compute_grid_def(x);
		
		// seteaza culoarea terenului
		unsigned int col_vec = glGetUniformLocation(gl_program_shader, "color_vector");
		float color[3] = {0.50f, 0.56f, 0.0f};
		glProgramUniform3fv(gl_program_shader, col_vec, 1, color);

		grid_num_indices = num_ind;

		glGenVertexArrays(1 , &grid_vao);
		glBindVertexArray(grid_vao);

		glGenBuffers(1, &grid_vbo);
		glBindBuffer(GL_ARRAY_BUFFER, grid_vbo);
		glBufferData(GL_ARRAY_BUFFER, sizeof(lab::VertexFormat) * size* size, &vertex[0], GL_STATIC_DRAW);

		glGenBuffers(1, &grid_ibo);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, grid_ibo);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * num_ind, &index[0], GL_STATIC_DRAW);

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(lab::VertexFormat), (void*) 0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(lab::VertexFormat), (void*) (sizeof(float)*3));
		delete[] vertex;
		delete[] index;
	}

	// deseneaza gridul plat
	void flat(unsigned int size)
	{
		float dim = 100.0 / size * 4;
		vertex = new lab::VertexFormat[size * size];
		
		const unsigned int num_ind = (size - 1) * (size - 1) * 6;
		index = new unsigned int[num_ind];
		float dim_x, dim_z = 0.0f;
		int indx_v = 0;
		int indx_i = 0;

		for (int i = 0; i < size; i++) {
			dim_x = 0;
			for (int j = 0; j < size; j++) {
				vertex[indx_v++] = lab::VertexFormat(dim_x, 0, dim_z);
				if ((i < (size - 1)) && (j < (size - 1)))
				{
					index[indx_i++] = (i + 1) * size + j;
					index[indx_i++] = i * size + j;
					index[indx_i++] = (i + 1) * size + j + 1;
					index[indx_i++] = (i + 1) * size + j + 1;
					index[indx_i++] = i * size + j;
					index[indx_i++] = i * size + j + 1;
				}
				dim_x += dim;
			}
			dim_z += dim;
		}
		
		grid_num_indices = num_ind;

		glGenVertexArrays(1 , &grid_vao);
		glBindVertexArray(grid_vao);

		glGenBuffers(1, &grid_vbo);
		glBindBuffer(GL_ARRAY_BUFFER, grid_vbo);
		glBufferData(GL_ARRAY_BUFFER, sizeof(lab::VertexFormat) * size* size, &vertex[0], GL_STATIC_DRAW);

		glGenBuffers(1, &grid_ibo);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, grid_ibo);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * num_ind, &index[0], GL_STATIC_DRAW);

		unsigned int col_vec = glGetUniformLocation(gl_program_shader, "color_vector");
		float color[3] = {0.15f, 0.56f, 0.12f};
		glProgramUniform3fv(gl_program_shader, col_vec, 1, color);

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,sizeof(lab::VertexFormat),(void*)0);						//trimite pozitii pe pipe 0
	}

	void draw()
	{
		glUniformMatrix4fv(glGetUniformLocation(gl_program_shader,"model_matrix"),1,false,glm::value_ptr(model_matrix_grid));
		glBindVertexArray(grid_vao);
		glDrawElements(GL_TRIANGLES, grid_num_indices, GL_UNSIGNED_INT, 0);
	}

	//--------------------------------------------------------------------------------------------
	//functii de cadru ---------------------------------------------------------------------------

	//functie chemata inainte de a incepe cadrul de desenare, o folosim ca sa updatam situatia scenei ( modelam/simulam scena)
	void notifyBeginFrame(){
		//rotatie
		static float angle=0;
		angle = 0.01f;
		glm::vec3 lp = light_position;
		
	}
	//functia de afisare (lucram cu banda grafica)
	void notifyDisplayFrame(){
		unsigned int locatie;
		//pe tot ecranul
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		//foloseste shaderul
		glUseProgram(gl_program_shader);
		
		//trimite variabile uniforme la shader
		glUniformMatrix4fv(glGetUniformLocation(gl_program_shader, "model_matrix"),1,false,glm::value_ptr(model_matrix_grid));
		glUniformMatrix4fv(glGetUniformLocation(gl_program_shader, "view_matrix"),1,false,glm::value_ptr(view_matrix));
		glUniformMatrix4fv(glGetUniformLocation(gl_program_shader, "projection_matrix"),1,false,glm::value_ptr(projection_matrix));
		glUniform3f(glGetUniformLocation(gl_program_shader, "light_position"),light_position.x, light_position.y, light_position.z);
		glUniform3f(glGetUniformLocation(gl_program_shader, "eye_position"),eye_position.x, eye_position.y, eye_position.z);
		glUniform1i(glGetUniformLocation(gl_program_shader, "material_shininess"),material_shininess);
		glUniform1f(glGetUniformLocation(gl_program_shader, "material_kd"),material_kd);
		glUniform1f(glGetUniformLocation(gl_program_shader, "material_ks"),material_ks);


		//bind obiect
		glm::mat4 matrice_translatie;

		//pune o sfera la pozitia luminii
		matrice_translatie = glm::translate(model_matrix_grid, glm::vec3(light_position.x,light_position.y,light_position.z));
		glm::mat4 matrice_scalare = glm::scale(model_matrix_grid, glm::vec3(0.1,0.1,0.1));
		glUniformMatrix4fv(glGetUniformLocation(gl_program_shader, "model_matrix"),1,false,glm::value_ptr(matrice_translatie * matrice_scalare));
		
		glBindVertexArray(mesh_vao_sphere);
		glDrawElements(GL_TRIANGLES, mesh_num_indices_sphere, GL_UNSIGNED_INT, 0);

		// grid 
		draw();
		
		camera_fps.render(position, &view_matrix);
	}
	//functie chemata dupa ce am terminat cadrul de desenare (poate fi folosita pt modelare/simulare)
	void notifyEndFrame(){}
	//functei care e chemata cand se schimba dimensiunea ferestrei initiale
	void notifyReshape(int width, int height, int previos_width, int previous_height){
		//reshape
		if(height==0) height=1;
		float aspect = (float)width / (float)height;
		projection_matrix = glm::perspective(75.0f, aspect,0.1f, 10000.0f);
	}


	//--------------------------------------------------------------------------------------------
	//functii de input output --------------------------------------------------------------------
	
	//tasta apasata
	void notifyKeyPressed(unsigned char key_pressed, int mouse_x, int mouse_y){
		glm::vec3 aux;
		float ang;
		static bool wire =true;
		switch(key_pressed) {	
			case 'w':
				aux = glm::vec3(position.x + distm*sin((-rotate_sum)*3.1415f/180.0),
								position.y,
								position.z - distm*cos((-rotate_sum)*3.1415f/180.0));
				position = aux;
				break;
			case 's':
				aux = glm::vec3(position.x - distm*sin((-rotate_sum)*3.1415f/180.0),
								position.y,
								position.z + distm*cos((-rotate_sum)*3.1415f/180.0));
				position = aux;
				break;
			case 'a':
				rotate_sum += ((rotate_angle)*180.0)/3.1415f;
				camera_fps.rotateFPS_OY(-rotate_angle);	
				break;
			case 'd':
				rotate_sum += ((-rotate_angle)*180.0)/3.1415f;
				camera_fps.rotateFPS_OY(rotate_angle);
				break;
			case 'q':
				position = glm::vec3(position.x, position.y + 1, position.z);
				break;
			case 'e':
				position = glm::vec3(position.x, position.y + 1, position.z);
				break;
			case 27 :
				lab::glut::close();
				break;
			case '5' :
				wire=!wire;
				glPolygonMode(GL_FRONT_AND_BACK, (wire?GL_LINE:GL_FILL));
				break;
			case '2': 
				grid_size *= 2;
				if (grid_size > 100) {
					grid_size = 25;
				}
				if (deformat != 0) {
					draw_grid_def(grid_size);
				} else {
					flat(grid_size);
				}
				break;
			case '3' :
				deformat = (deformat ==  0 ? 1 : 0);
				if (deformat != 0) {
					draw_grid_def(grid_size);
				} else {
					flat(grid_size);
				}
				break;
			case 'h' :
				light_position = light_position + glm::vec3(-1.0f, 0.0f, 0.0f);
				break;
			case 'k' :
				light_position = light_position + glm::vec3(1.0f, 0.0f, 0.0f);
				break;
			case 'u' :
				light_position = light_position + glm::vec3(0.0f, 1.0f, 0.0f);
				break;
			case 'j' :
				light_position = light_position + glm::vec3(0.0f, -1.0f, 0.0f);
				break;
			case 'n' :
				light_position = light_position + glm::vec3(0.0f, 0.0f, 1.0f);
				break;
			case 'm' :
				light_position = light_position + glm::vec3(0.0f, 0.0f, -1.0f);
				break;
			case 32 :
				glDeleteProgram(gl_program_shader);
				gl_program_shader = lab::loadShader("shadere\\shader_vertex.glsl", "shadere\\shader_fragment.glsl");
				break;
			default:
				break;
		}
	}
	//tasta ridicata
	void notifyKeyReleased(unsigned char key_released, int mouse_x, int mouse_y){	}
	//tasta speciala (up/down/F1/F2..) apasata
	void notifySpecialKeyPressed(int key_pressed, int mouse_x, int mouse_y){
		if(key_pressed == GLUT_KEY_F1) lab::glut::enterFullscreen();
		if(key_pressed == GLUT_KEY_F2) lab::glut::exitFullscreen();
	}
	//tasta speciala ridicata
	void notifySpecialKeyReleased(int key_released, int mouse_x, int mouse_y){}
	//drag cu mouse-ul
	void notifyMouseDrag(int mouse_x, int mouse_y){ }
	//am miscat mouseul (fara sa apas vreun buton)
	void notifyMouseMove(int mouse_x, int mouse_y){ }
	//am apasat pe un boton
	void notifyMouseClick(int button, int state, int mouse_x, int mouse_y){ }
	//scroll cu mouse-ul
	void notifyMouseScroll(int wheel, int direction, int mouse_x, int mouse_y){ }

};

int main(){
	//initializeaza GLUT (fereastra + input + context OpenGL)
	lab::glut::WindowInfo window(std::string("lab shadere 5 - texturi"),800,600,100,100,true);
	lab::glut::ContextInfo context(3,3,false);
	lab::glut::FramebufferInfo framebuffer(true,true,true,true);
	lab::glut::init(window,context, framebuffer);

	//initializeaza GLEW (ne incarca functiile openGL, altfel ar trebui sa facem asta manual!)
	glewExperimental = true;
	glewInit();
	std::cout<<"GLEW:initializare"<<std::endl;

	//creem clasa noastra si o punem sa asculte evenimentele de la GLUT
	//DUPA GLEW!!! ca sa avem functiile de OpenGL incarcate inainte sa ii fie apelat constructorul (care creeaza obiecte OpenGL)
	Laborator mylab;
	lab::glut::setListener(&mylab);

	//taste
	std::cout<<"Taste:"<<std::endl<<"\tESC ... iesire"<<std::endl<<"\tSPACE ... reincarca shadere"<<std::endl<<"\tw ... toggle wireframe"<<std::endl;

	//run
	lab::glut::run();

	return 0;
}