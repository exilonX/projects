#include "camera.h"

Camera::Camera(){
}
Camera::~Camera(){
}

void Camera::init(){
	position = glm::vec3(0,0, 0);
	forward = glm::vec3(0,0,-1);
	up = glm::vec3(0,1,0);
	right = glm::vec3(1,0,0);
}

void Camera::translate_Forward(float dist){
	position = position + forward * dist;
}

void Camera::translate_Up(float dist){
	position = position + up * dist;
}

void Camera::translate_Right(float dist){
	position = position + right * dist;
}


void Camera::rotateFPS_OY(float angle){
	forward = forward * cos(angle) + right * sin(angle);
	//right = forward.CrossProduct(up);
	right = glm::cross(forward, up);
}

void Camera::rotateFPS_OX(float angle){
	right = right * cos(angle) + up * sin(angle);
	//up = right.CrossProduct(forward);
	up = glm::cross(right, forward);
}

void Camera::rotateFPS_OZ(float angle){
	up = up * cos(angle) + forward * sin(angle);
	//forward = up.CrossProduct(right);
	forward = glm::cross(up, right);
}

void Camera::rotateTPS_OY(float angle, float dist_to_interes){
	translate_Forward(dist_to_interes);
	rotateFPS_OY(angle);
	translate_Forward(-dist_to_interes);
}

void Camera::rotateTPS_OX(float angle, float dist_to_interes){	
	translate_Forward(dist_to_interes);
	rotateFPS_OX(angle);
	translate_Forward(-dist_to_interes);
}

void Camera::rotateTPS_OZ(float angle, float dist_to_interes){
	translate_Forward(dist_to_interes);
	rotateFPS_OZ(angle);
	translate_Forward(-dist_to_interes);
}

void Camera::render(glm::vec3 pos, glm::mat4 *view_matrix){
	glm::vec3 center = pos + forward;
	(*view_matrix) = glm::lookAt(pos, center, up);
}

void Camera::render(glm::vec3 pos, glm::vec3 cent, glm::mat4 *view_matrix){
	glm::vec3 center = pos + forward;
	(*view_matrix) = glm::lookAt(pos, cent, up);
}

void Camera::render(glm::mat4 *view_matrix){
	glm::vec3 center = position + forward;
	(*view_matrix) = glm::lookAt(position, center, up);
}