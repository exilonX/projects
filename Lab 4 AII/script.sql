CREATE DATABASE Grupa341C4_MercaIonel;

USE Grupa341C4_MercaIonel;

CREATE TABLE departament (
	id_departament	INT(2) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
	denumire	enum('programare', 'Q&A', 'contabilitate', 'resurse umane') NOT NULL
	#resp_dep BIGINT(13) UNSIGNED NOT NULL,
    #FOREIGN KEY (resp_dep) REFERENCES utilizatori (CNP) ON UPDATE CASCADE ON DELETE CASCADE
);

ALTER TABLE departament ADD CONSTRAINT denumire_chk CHECK (denumire IN ('programare', 'Q&A',
 'contabilitate', 'resurse umane'));

insert into departament (id_departament, denumire) 
    values (default, 'programare');

insert into departament (id_departament, denumire) 
    values (default, 'resurse umane');

insert into departament (id_departament, denumire) 
    values (default, 'Q&A');

insert into departament (id_departament, denumire) 
    values (default, 'contabilitate');
insert into departament (id_departament, denumire)
    values (default, 'admin');

CREATE TABLE utilizatori (
	CNP			BIGINT(13) UNSIGNED PRIMARY KEY NOT NULL,
	nume			VARCHAR(30) NOT NULL,
	prenume			VARCHAR(30) NOT NULL,
	adresa			VARCHAR(100) NOT NULL,
	telefon			INT(10),
	email			VARCHAR(60) NOT NULL,
    	IBAN           		VARCHAR(30) NOT NULL,
    	nr_contract     	INT(10) NOT NULL,
   	 data_angajarii  	DATETIME NOT NULL,
    	functia         	VARCHAR(60) NOT NULL,
    	nume_utilizator 	VARCHAR(20) NOT NULL,
    	parola          	VARCHAR(20) NOT NULL,
	tip		        enum('admin', 'super_admin', 'angajat') NOT NULL DEFAULT 'angajat',
    	salariu_n       	INT(6) NOT NULL,
    	id_departament  	INT(2) UNSIGNED NOT NULL,
	ore_contract		INT(2),
    	FOREIGN KEY (id_departament) REFERENCES departament (id_departament) ON UPDATE CASCADE ON DELETE CASCADE
);

ALTER TABLE utilizatori ADD CONSTRAINT email_chk CHECK (email LIKE '%@%.%');
ALTER TABLE utilizatori ADD CONSTRAINT tip_chk CHECK (tip IN ('admin', 'super_admin', 'angajat'));


INSERT INTO utilizatori (CNP, nume, prenume, adresa, telefon, email, IBAN, 
    nr_contract, data_angajarii, functia, nume_utilizator, 
    parola, tip, salariu_n, id_departament, ore_contract)
 VALUES('1320713372318', 'MARICA', 'Ionel', '-', '0', 'marica.ionel@google.com', 
'RO135AW342353312123', '2312', '1997-3-11', 'admin', 'marica.ionel', 'a',
 'admin',  '2300', '5', '240');



INSERT INTO utilizatori (CNP, nume, prenume, adresa, telefon, email, IBAN, 
    nr_contract, data_angajarii, functia, nume_utilizator, 
    parola, tip, salariu_n, id_departament, ore_contract)
 VALUES('1700713372323', 'NEAGU', 'Vasile', '-', '0', 'neagu.vasile@google.com', 
'RO135AW3421314124124', '2222', '1999-5-11', 'super-admin', 'neagu.vasile', 'a',
 'super_admin',  '2300', '5', '240');
INSERT INTO utilizatori (CNP, nume, prenume, adresa, telefon, email, IBAN, 
    nr_contract, data_angajarii, functia, nume_utilizator, 
    parola, tip, salariu_n, id_departament, ore_contract)
 VALUES('1220713379378', 'ZOTA', 'Daniel', '-', '0', 'zota.daniel@google.com', 
'RO135AWJQI1H4J12H41J', '231', '1992-3-11', 'programator', 'zota.daniel', 'a',
 'angajat',  '2300', '1', '240');



INSERT INTO utilizatori (CNP, nume, prenume, adresa, telefon, email, IBAN, 
    nr_contract, data_angajarii, functia, nume_utilizator, 
    parola, tip, salariu_n, id_departament, ore_contract)
 VALUES('1160121873449', 'NEGREANU', 'Mircea', '-', '0', 'negreanu.mircea@hotmail.com',
 'RO135AWJQJEQKWEJQ', '142', '2002-4-11', 'programator', 'negreanu.mircea', 'a',
 'angajat', '2500', '1', '240');

INSERT INTO utilizatori (CNP, nume, prenume, adresa, telefon, email, IBAN, 
    nr_contract, data_angajarii, functia, nume_utilizator, 
    parola, tip, salariu_n, id_departament, ore_contract)
 VALUES('1940503835624', 'DRAGUSIN', 'Teodora', '-', '0', 'dragusin.teodora@oracle.com',
'RO135AWJ14H12J4HJ1K2', '152', '2001-4-11', 'programator', 'dragusin.teodora', 'a',
 'angajat', '2500', '1', '240');


INSERT INTO utilizatori (CNP, nume, prenume, adresa, telefon, email, IBAN, 
    nr_contract, data_angajarii, functia, nume_utilizator, 
    parola, tip, salariu_n, id_departament, ore_contract)
 VALUES('1560330668692', 'ROGOBETE', 'Mircea', '-', '0', 'rogobete.mircea@live.com',
 'RO135AWJQUIWEYQUIWEY', '144', '2004-5-1', 'programator', 'rogobete.mircea', 'a',
 'angajat', '2800', '1', '240');




INSERT INTO utilizatori (CNP, nume, prenume, adresa, telefon, email, IBAN, 
    nr_contract, data_angajarii, functia, nume_utilizator, 
    parola, tip, salariu_n, id_departament, ore_contract)
 VALUES('2981007225570', 'GIUMALE', 'Petre', '-', '0', 'giumale.petre@yahoo.com',
 'RO1352K1J42412312414', '333', '2005-10-1', 'resurse umane', 'giumale.petre', 'a',
 'angajat', '2800', '2', '240');

INSERT INTO utilizatori (CNP, nume, prenume, adresa, telefon, email, IBAN, 
    nr_contract, data_angajarii, functia, nume_utilizator, 
    parola, tip, salariu_n, id_departament, ore_contract)
 VALUES('1350615180258', 'DRAGUSIN', 'Camelia', '-', '0', 'dragusin.camelia@live.com',
 'RO1352K1J44H12HJK23', '433', '2005-10-1', 'resurse umane', 'dragusin.camelia', 'a',
 'angajat', '1800', '2', '240');

INSERT INTO utilizatori (CNP, nume, prenume, adresa, telefon, email, IBAN, 
    nr_contract, data_angajarii, functia, nume_utilizator, 
    parola, tip, salariu_n, id_departament, ore_contract)
 VALUES('1860926240323', 'IONESCU', 'Tudor', '-', '0', 'ionescu.tudor@google.com', 
 'RO1352K1J412KJ3L123', '341', '2010-09-1', 'qa', 'ionescu.tudor', 'a',
 'angajat', '2000', '3', '240');

INSERT INTO utilizatori (CNP, nume, prenume, adresa, telefon, email, IBAN, 
    nr_contract, data_angajarii, functia, nume_utilizator, 
    parola, tip, salariu_n, id_departament, ore_contract)
 VALUES('1921202602499', 'GEORGESCU', 'Monica', '-', '0', 'georgescu.monica@yahoo.com',
  'RO1352K1J412KJ3L123', '341', '2010-09-1', 'qa', 'georgescu.monica', 'a',
 'angajat', '3800', '3', '240');



INSERT INTO utilizatori (CNP, nume, prenume, adresa, telefon, email, IBAN, 
    nr_contract, data_angajarii, functia, nume_utilizator, 
    parola, tip, salariu_n, id_departament, ore_contract)
 VALUES('1320810148322', 'POPA', 'Monica', '-', '0', 'popa.monica@yahoo.com',
 'RO1352K1J4121451512', '371', '2012-09-1', 'qa', 'popa.monica', 'a',
 'angajat', '1800', '3', '240');

INSERT INTO utilizatori (CNP, nume, prenume, adresa, telefon, email, IBAN, 
    nr_contract, data_angajarii, functia, nume_utilizator, 
    parola, tip, salariu_n, id_departament, ore_contract)
 VALUES('2390831245242', 'CHIVU', 'Camelia', '-', '0', 'chivu.camelia@yahoo.com',
 'RO1352K1J4121251DAKJ2', '351', '2012-09-1', 'qa', 'chivu.camelia', 'a',
 'angajat', '1100', '3', '240');



INSERT INTO utilizatori (CNP, nume, prenume, adresa, telefon, email, IBAN, 
    nr_contract, data_angajarii, functia, nume_utilizator, 
    parola, tip, salariu_n, id_departament, ore_contract)
 VALUES('1400515716087', 'POPA', 'Cristina', '-', '0', 'popa.cristina@yahoo.com',
 'RO1352K1J4JK14124J12K4', '451', '2013-01-11', 'contabilitate', 'popa.cristina', 'a',
 'angajat', '1100', '4', '240');


INSERT INTO utilizatori (CNP, nume, prenume, adresa, telefon, email, IBAN, 
    nr_contract, data_angajarii, functia, nume_utilizator, 
    parola, tip, salariu_n, id_departament, ore_contract)
 VALUES('2740606757054', 'VASILESCU', 'Radu', '-', '0', 'vasilescu.radu@google.com',
  'RO1352K1J4JK21213J4H2', '411', '2000-01-11', 'contabilitate', 'vasilescu.radu', 'a',
 'angajat', '4100', '4', '232');




CREATE table asoc_responsabil_departament (
    id_asoc             INT(2) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
    id_utilizator       BIGINT(13) UNSIGNED NOT NULL,
    id_departament      INT(2) UNSIGNED NOT NULL,
    FOREIGN KEY (id_utilizator) REFERENCES utilizatori (CNP) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (id_departament) REFERENCES departament (id_departament) ON UPDATE CASCADE ON DELETE CASCADE
);

insert into asoc_responsabil_departament (id_asoc, id_utilizator, id_departament) 
    values (default, '1220713379378', 1);
insert into asoc_responsabil_departament (id_asoc, id_utilizator, id_departament) 
    values (default, '2981007225570', 2);
insert into asoc_responsabil_departament (id_asoc, id_utilizator, id_departament) 
    values (default, '1320810148322', 3);
insert into asoc_responsabil_departament (id_asoc, id_utilizator, id_departament) 
    values (default, '1400515716087', 4);





CREATE TABLE echipa (
	id_echipa	INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
	denumire	VARCHAR(30) NOT NULL,
    id_resp     BIGINT(13) UNSIGNED NOT NULL,
    id_dept     INT(2) UNSIGNED NOT NULL,
    FOREIGN KEY (id_resp) REFERENCES utilizatori (CNP) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (id_dept) REFERENCES departament (id_departament) ON UPDATE CASCADE ON DELETE CASCADE
);

insert into echipa (id_echipa, denumire, id_resp, id_dept) 
    values (default, 'programare1', '1220713379378', '1');
insert into echipa (id_echipa, denumire, id_resp, id_dept) 
    values (default, 'programare2', '1160121873449', '1');
insert into echipa (id_echipa, denumire, id_resp, id_dept) 
    values (default, 'qa1', '1860926240323', '3');
insert into echipa (id_echipa, denumire, id_resp, id_dept) 
    values (default, 'qa2', '1921202602499', '3');



CREATE TABLE asoc_echipa_utiliz (
    id_asoc     INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
	id_echipa	INT(10) UNSIGNED NOT NULL,
    id_utiliz   BIGINT(13) UNSIGNED NOT NULL,
    data_start  DATE NOT NULL,
    data_final  DATE NOT NULL,
    FOREIGN KEY (id_echipa) REFERENCES echipa (id_echipa) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (id_utiliz) REFERENCES utilizatori (CNP) ON UPDATE CASCADE ON DELETE CASCADE
);

insert into asoc_echipa_utiliz (id_asoc, id_echipa, id_utiliz, data_start, data_final) 
    values (default, '1', '1560330668692', '2013-01-1', '2013-09-1');

insert into asoc_echipa_utiliz (id_asoc, id_echipa, id_utiliz,data_start, data_final) 
    values (default, '2', '1940503835624', '2013-04-1', '2013-06-1');

insert into asoc_echipa_utiliz (id_asoc, id_echipa, id_utiliz, data_start, data_final) 
    values (default, '3', '2390831245242', '2013-10-1', '2013-12-1');

insert into asoc_echipa_utiliz (id_asoc, id_echipa, id_utiliz, data_start, data_final) 
    values (default, '4', '1320810148322', '2013-10-1', '2014-01-1');

insert into asoc_echipa_utiliz (id_asoc, id_echipa, id_utiliz, data_start, data_final) 
    values (default, '1', '1220713379378', '2013-01-1', '2013-09-1');

insert into asoc_echipa_utiliz (id_asoc, id_echipa, id_utiliz, data_start, data_final) 
    values (default, '2', '1160121873449', '2013-04-1', '2013-06-1');

insert into asoc_echipa_utiliz (id_asoc, id_echipa, id_utiliz, data_start, data_final) 
    values (default, '3', '1860926240323', '2013-10-1', '2013-12-1');

insert into asoc_echipa_utiliz (id_asoc, id_echipa, id_utiliz, data_start, data_final) 
    values (default, '4',  '1921202602499', '2013-10-1', '2014-01-1');

CREATE TABLE activitate (
	id_intrare	INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
    id_utiliz   BIGINT(13) UNSIGNED NOT NULL,
    data        DATE NOT NULL,
    ora_sosire  TIME,
    ora_plecare TIME,
    concediu    enum('NONE', 'odihna', 'medical', 'neplatit', 'special') NOT NULL DEFAULT 'NONE',
    FOREIGN KEY (id_utiliz) REFERENCES utilizatori (CNP) ON UPDATE CASCADE ON DELETE CASCADE
);

delimiter //
CREATE TRIGGER chk_concediu
BEFORE INSERT ON activitate
FOR EACH ROW
BEGIN
    declare msg varchar(100);
    IF ((NEW.ora_sosire = null and  NEW.ora_plecare = null) and NEW.concediu = 'NONE') THEN
        set NEW.concediu = 'special';
  END IF;
END; //
delimiter ;

insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1220713379378', '2012-09-1', '00:00', '00:00', 'odihna');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1220713379378', '2012-09-2', '00:00', '00:00', 'odihna');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1220713379378', '2012-09-3', '00:00', '00:00', 'odihna');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1220713379378', '2012-09-4', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1220713379378', '2012-09-5', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1220713379378', '2012-09-6', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1220713379378', '2012-09-7', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1220713379378', '2012-09-8', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1220713379378', '2012-09-9', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1220713379378', '2012-09-10', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1220713379378', '2012-09-11', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1220713379378', '2012-09-12', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1220713379378', '2012-09-13', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1220713379378', '2012-09-14', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1220713379378', '2012-09-15', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1220713379378', '2012-09-16', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1220713379378', '2012-09-17', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1220713379378', '2012-09-18', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1220713379378', '2012-09-19', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1220713379378', '2012-09-20', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1220713379378', '2012-09-21', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1220713379378', '2012-09-22', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1220713379378', '2012-09-23', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1220713379378', '2012-09-24', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1220713379378', '2012-09-25', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1220713379378', '2012-09-26', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1220713379378', '2012-09-27', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1220713379378', '2012-09-28', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1220713379378', '2012-09-29', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1220713379378', '2012-09-30', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1160121873449', '2012-09-1', '00:00', '00:00', 'special');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1160121873449', '2012-09-2', '00:00', '00:00','odihna');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1160121873449', '2012-09-3', '00:00', '00:00', 'odihna');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1160121873449', '2012-09-4', '09:00', '20:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1160121873449', '2012-09-5', '09:00', '20:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1160121873449', '2012-09-6', '09:00', '20:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1160121873449', '2012-09-7', '09:00', '16:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1160121873449', '2012-09-8', '09:00', '19:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1160121873449', '2012-09-9', '10:00', '19:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1160121873449', '2012-09-10', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1160121873449', '2012-09-11', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1160121873449', '2012-09-12', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1160121873449', '2012-09-13', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1160121873449', '2012-09-14', '00:00', '00:00', 'odihna');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1160121873449', '2012-09-15', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1160121873449', '2012-09-16', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1160121873449', '2012-09-17', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1160121873449', '2012-09-18', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1160121873449', '2012-09-19', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1160121873449', '2012-09-20', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1160121873449', '2012-09-21', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1160121873449', '2012-09-22', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1160121873449', '2012-09-23', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1160121873449', '2012-09-24', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1160121873449', '2012-09-25', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1160121873449', '2012-09-26', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1160121873449', '2012-09-27', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1160121873449', '2012-09-28', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1160121873449', '2012-09-29', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1160121873449', '2012-09-30', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1940503835624', '2012-09-1', '00:00', '00:00', 'neplatit');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1940503835624', '2012-09-2', '00:00', '00:00', 'odihna');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1940503835624', '2012-09-3', '00:00', '00:00', 'special');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1940503835624', '2012-09-4', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1940503835624', '2012-09-5', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1940503835624', '2012-09-6', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1940503835624', '2012-09-7', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1940503835624', '2012-09-8', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1940503835624', '2012-09-9', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1940503835624', '2012-09-10', '00:00', '00:00', 'odihna');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1940503835624', '2012-09-11', '00:00', '00:00', 'neplatit');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1940503835624', '2012-09-12', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1940503835624', '2012-09-13', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1940503835624', '2012-09-14', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1940503835624', '2012-09-15', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1940503835624', '2012-09-16', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1940503835624', '2012-09-17', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1940503835624', '2012-09-18', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1940503835624', '2012-09-19', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1940503835624', '2012-09-20', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1940503835624', '2012-09-21', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1940503835624', '2012-09-22', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1940503835624', '2012-09-23', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1940503835624', '2012-09-24', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1940503835624', '2012-09-25', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1940503835624', '2012-09-26', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1940503835624', '2012-09-27', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1940503835624', '2012-09-28', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1940503835624', '2012-09-29', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1940503835624', '2012-09-30', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1560330668692', '2012-09-1', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1560330668692', '2012-09-2', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1560330668692', '2012-09-3', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1560330668692', '2012-09-4', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1560330668692', '2012-09-5', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1560330668692', '2012-09-6', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1560330668692', '2012-09-7', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1560330668692', '2012-09-8', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1560330668692', '2012-09-9', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1560330668692', '2012-09-10', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1560330668692', '2012-09-11', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1560330668692', '2012-09-12', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1560330668692', '2012-09-13', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1560330668692', '2012-09-14', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1560330668692', '2012-09-15', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1560330668692', '2012-09-16', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1560330668692', '2012-09-17', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1560330668692', '2012-09-18', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1560330668692', '2012-09-19', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1560330668692', '2012-09-20', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1560330668692', '2012-09-21', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1560330668692', '2012-09-22', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1560330668692', '2012-09-23', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1560330668692', '2012-09-24', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1560330668692', '2012-09-25', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1560330668692', '2012-09-26', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1560330668692', '2012-09-27', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1560330668692', '2012-09-28', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1560330668692', '2012-09-29', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1560330668692', '2012-09-30', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2981007225570', '2012-09-1', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2981007225570', '2012-09-2', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2981007225570', '2012-09-3', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2981007225570', '2012-09-4', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2981007225570', '2012-09-5', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2981007225570', '2012-09-6', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2981007225570', '2012-09-7', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2981007225570', '2012-09-8', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2981007225570', '2012-09-9', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2981007225570', '2012-09-10', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2981007225570', '2012-09-11', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2981007225570', '2012-09-12', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2981007225570', '2012-09-13', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2981007225570', '2012-09-14', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2981007225570', '2012-09-15', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2981007225570', '2012-09-16', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2981007225570', '2012-09-17', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2981007225570', '2012-09-18', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2981007225570', '2012-09-19', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2981007225570', '2012-09-20', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2981007225570', '2012-09-21', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2981007225570', '2012-09-22', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2981007225570', '2012-09-23', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2981007225570', '2012-09-24', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2981007225570', '2012-09-25', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2981007225570', '2012-09-26', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2981007225570', '2012-09-27', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2981007225570', '2012-09-28', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2981007225570', '2012-09-29', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2981007225570', '2012-09-30', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1350615180258', '2012-09-1', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1350615180258', '2012-09-2', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1350615180258', '2012-09-3', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1350615180258', '2012-09-4', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1350615180258', '2012-09-5', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1350615180258', '2012-09-6', '00:00', '00:00', 'odihna');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1350615180258', '2012-09-7', '00:00', '00:00', 'odihna');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1350615180258', '2012-09-8', '00:00', '00:00', 'odihna');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1350615180258', '2012-09-9', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1350615180258', '2012-09-10', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1350615180258', '2012-09-11', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1350615180258', '2012-09-12', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1350615180258', '2012-09-13', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1350615180258', '2012-09-14', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1350615180258', '2012-09-15', '00:00', '00:00', 'neplatit');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1350615180258', '2012-09-16', '00:00', '00:00', 'neplatit');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1350615180258', '2012-09-17', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1350615180258', '2012-09-18', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1350615180258', '2012-09-19', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1350615180258', '2012-09-20', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1350615180258', '2012-09-21', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1350615180258', '2012-09-22', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1350615180258', '2012-09-23', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1350615180258', '2012-09-24', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1350615180258', '2012-09-25', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1350615180258', '2012-09-26', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1350615180258', '2012-09-27', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1350615180258', '2012-09-28', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1350615180258', '2012-09-29', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1350615180258', '2012-09-30', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1860926240323', '2012-09-1', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1860926240323', '2012-09-2', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1860926240323', '2012-09-3', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1860926240323', '2012-09-4', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1860926240323', '2012-09-5', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1860926240323', '2012-09-6', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1860926240323', '2012-09-7', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1860926240323', '2012-09-8', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1860926240323', '2012-09-9', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1860926240323', '2012-09-10', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1860926240323', '2012-09-11', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1860926240323', '2012-09-12', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1860926240323', '2012-09-13', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1860926240323', '2012-09-14', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1860926240323', '2012-09-15', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1860926240323', '2012-09-16', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1860926240323', '2012-09-17', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1860926240323', '2012-09-18', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1860926240323', '2012-09-19', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1860926240323', '2012-09-20', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1860926240323', '2012-09-21', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1860926240323', '2012-09-22', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1860926240323', '2012-09-23', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1860926240323', '2012-09-24', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1860926240323', '2012-09-25', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1860926240323', '2012-09-26', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1860926240323', '2012-09-27', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1860926240323', '2012-09-28', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1860926240323', '2012-09-29', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1860926240323', '2012-09-30', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1921202602499', '2012-09-1', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1921202602499', '2012-09-2', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1921202602499', '2012-09-3', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1921202602499', '2012-09-4', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1921202602499', '2012-09-5', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1921202602499', '2012-09-6', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1921202602499', '2012-09-7', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1921202602499', '2012-09-8', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1921202602499', '2012-09-9', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1921202602499', '2012-09-10', '00:00', '00:00', 'medical');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1921202602499', '2012-09-11', '00:00', '00:00', 'medical');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1921202602499', '2012-09-12', '00:00', '00:00', 'medical');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1921202602499', '2012-09-13', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1921202602499', '2012-09-14', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1921202602499', '2012-09-15', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1921202602499', '2012-09-16', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1921202602499', '2012-09-17', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1921202602499', '2012-09-18', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1921202602499', '2012-09-19', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1921202602499', '2012-09-20', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1921202602499', '2012-09-21', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1921202602499', '2012-09-22', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1921202602499', '2012-09-23', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1921202602499', '2012-09-24', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1921202602499', '2012-09-25', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1921202602499', '2012-09-26', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1921202602499', '2012-09-27', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1921202602499', '2012-09-28', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1921202602499', '2012-09-29', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1921202602499', '2012-09-30', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1320810148322', '2012-09-1', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1320810148322', '2012-09-2', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1320810148322', '2012-09-3', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1320810148322', '2012-09-4', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1320810148322', '2012-09-5', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1320810148322', '2012-09-6', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1320810148322', '2012-09-7', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1320810148322', '2012-09-8', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1320810148322', '2012-09-9', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1320810148322', '2012-09-10', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1320810148322', '2012-09-11', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1320810148322', '2012-09-12', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1320810148322', '2012-09-13', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1320810148322', '2012-09-14', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1320810148322', '2012-09-15', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1320810148322', '2012-09-16', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1320810148322', '2012-09-17', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1320810148322', '2012-09-18', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1320810148322', '2012-09-19', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1320810148322', '2012-09-20', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1320810148322', '2012-09-21', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1320810148322', '2012-09-22', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1320810148322', '2012-09-23', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1320810148322', '2012-09-24', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1320810148322', '2012-09-25', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1320810148322', '2012-09-26', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1320810148322', '2012-09-27', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1320810148322', '2012-09-28', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1320810148322', '2012-09-29', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1320810148322', '2012-09-30', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2390831245242', '2012-09-1', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2390831245242', '2012-09-2', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2390831245242', '2012-09-3', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2390831245242', '2012-09-4', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2390831245242', '2012-09-5', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2390831245242', '2012-09-6', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2390831245242', '2012-09-7', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2390831245242', '2012-09-8', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2390831245242', '2012-09-9', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2390831245242', '2012-09-10', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2390831245242', '2012-09-11', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2390831245242', '2012-09-12', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2390831245242', '2012-09-13', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2390831245242', '2012-09-14', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2390831245242', '2012-09-15', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2390831245242', '2012-09-16', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2390831245242', '2012-09-17', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2390831245242', '2012-09-18', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2390831245242', '2012-09-19', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2390831245242', '2012-09-20', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2390831245242', '2012-09-21', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2390831245242', '2012-09-22', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2390831245242', '2012-09-23', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2390831245242', '2012-09-24', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2390831245242', '2012-09-25', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2390831245242', '2012-09-26', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2390831245242', '2012-09-27', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2390831245242', '2012-09-28', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2390831245242', '2012-09-29', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'2390831245242', '2012-09-30', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1400515716087', '2012-09-1', '00:00', '00:00', 'odihna');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1400515716087', '2012-09-2', '00:00', '00:00', 'odihna');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1400515716087', '2012-09-3', '00:00', '00:00', 'odihna');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1400515716087', '2012-09-4', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1400515716087', '2012-09-5', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1400515716087', '2012-09-6', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1400515716087', '2012-09-7', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1400515716087', '2012-09-8', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1400515716087', '2012-09-9', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1400515716087', '2012-09-10', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1400515716087', '2012-09-11', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1400515716087', '2012-09-12', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1400515716087', '2012-09-13', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1400515716087', '2012-09-14', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1400515716087', '2012-09-15', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1400515716087', '2012-09-16', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1400515716087', '2012-09-17', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1400515716087', '2012-09-18', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1400515716087', '2012-09-19', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1400515716087', '2012-09-20', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1400515716087', '2012-09-21', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1400515716087', '2012-09-22', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1400515716087', '2012-09-23', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1400515716087', '2012-09-24', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1400515716087', '2012-09-25', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1400515716087', '2012-09-26', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1400515716087', '2012-09-27', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1400515716087', '2012-09-28', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1400515716087', '2012-09-29', '09:00', '18:00', 'NONE');
insert into activitate (id_intrare, id_utiliz, data, ora_sosire, ora_plecare, concediu) values (default,'1400515716087', '2012-09-30', '09:00', '18:00', 'NONE');

CREATE TABLE facturi (
    id_factura      INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
    data_emitere    DATE NOT NULL,
    numar_factura   INT(10) UNSIGNED,
    suma            INT(10) UNSIGNED
);

insert into facturi (id_factura, data_emitere, numar_factura, suma) values
    (default, '2012-08-21', '23123', '23000');
insert into facturi (id_factura, data_emitere, numar_factura, suma) values
    (default, '2012-08-21', '73849', '20000');
insert into facturi (id_factura, data_emitere, numar_factura, suma) values
    (default, '2012-08-22', '61833', '55500');
insert into facturi (id_factura, data_emitere, numar_factura, suma) values
    (default, '2012-08-1', '66663', '61000');
insert into facturi (id_factura, data_emitere, numar_factura, suma) values
    (default, '2012-08-11', '23418', '2550');

insert into facturi (id_factura, data_emitere, numar_factura, suma) values
    (default, '2012-08-10', '100111', '2100');
insert into facturi (id_factura, data_emitere, numar_factura, suma) values
    (default, '2012-08-20', '23123', '9000');
insert into facturi (id_factura, data_emitere, numar_factura, suma) values
    (default, '2012-08-29', '33301', '3000');
insert into facturi (id_factura, data_emitere, numar_factura, suma) values
    (default, '2012-08-17', '26563', '22000');


CREATE TABLE proiect (
	id_proiect	INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
    denumire	VARCHAR(100) NOT NULL,
    descriere   VARCHAR(100) NOT NULL
);

insert into proiect (id_proiect, denumire, descriere) 
    values (default, 'proiect1', 'acest este un proiect erp 1');
insert into proiect (id_proiect, denumire, descriere) 
    values (default, 'proiect2', 'acest este un proiect erp 2');


CREATE TABLE asoc_factura_proiect (
	id_asoc_fp	INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
	id_factura  INT(10) UNSIGNED NOT NULL,
    id_proiect  INT(10) UNSIGNED NOT NULL,
    FOREIGN KEY (id_factura) REFERENCES facturi (id_factura) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (id_proiect) REFERENCES proiect (id_proiect) ON UPDATE CASCADE ON DELETE CASCADE
);

insert into asoc_factura_proiect (id_asoc_fp, id_factura, id_proiect)
    values (default, '1', '1');
insert into asoc_factura_proiect (id_asoc_fp, id_factura, id_proiect)
    values (default, '2', '1');
insert into asoc_factura_proiect (id_asoc_fp, id_factura, id_proiect)
    values (default, '3', '1');
insert into asoc_factura_proiect (id_asoc_fp, id_factura, id_proiect)
    values (default, '4', '1');
insert into asoc_factura_proiect (id_asoc_fp, id_factura, id_proiect)
    values (default, '5', '1');
insert into asoc_factura_proiect (id_asoc_fp, id_factura, id_proiect)
    values (default, '6', '2');
insert into asoc_factura_proiect (id_asoc_fp, id_factura, id_proiect)
    values (default, '7', '2');
insert into asoc_factura_proiect (id_asoc_fp, id_factura, id_proiect)
    values (default, '8', '2');
insert into asoc_factura_proiect (id_asoc_fp, id_factura, id_proiect)
    values (default, '9', '2');

CREATE TABLE asociere_proiect_deadline (
	id_asoc 	INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
	id_proiect	INT(10) UNSIGNED NOT NULL,
	id_echipa	INT(10) UNSIGNED NOT NULL,
    versiune    VARCHAR(10) NOT NULL,
    data_start  DATE NOT NULL,
    data_final  DATE NOT NULL,
	FOREIGN KEY (id_proiect) REFERENCES proiect (id_proiect) ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (id_echipa) REFERENCES echipa (id_echipa) ON UPDATE CASCADE ON DELETE CASCADE
);

delimiter //
CREATE TRIGGER chk_data_asoc
BEFORE INSERT ON asociere_proiect_deadline
FOR EACH ROW
BEGIN
    IF (NEW.data_start > NEW.data_final) THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ora de sosire mai mica decat ora de plecare';
  END IF;
END; //
delimiter ;

insert into asociere_proiect_deadline 
    (id_asoc, id_proiect, id_echipa, versiune, data_start, data_final) values
    (default, '1', '1', '1.0.1.2', '2013-01-1', '2013-09-1');
insert into asociere_proiect_deadline 
    (id_asoc, id_proiect, id_echipa, versiune, data_start, data_final) values
    (default, '2', '2', '2.0.1.2', '2013-03-1', '2013-9-1');
insert into asociere_proiect_deadline 
    (id_asoc, id_proiect, id_echipa, versiune, data_start, data_final) values
    (default, '1', '3', '1.0.1.2', '2013-11-1', '2013-12-1');
insert into asociere_proiect_deadline 
    (id_asoc, id_proiect, id_echipa, versiune, data_start, data_final) values
    (default, '2', '4', '2.0.1.2', '2013-9-1', '2013-12-1');


CREATE TABLE bugs (
	id_bug     	    INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
    denumire_bug    VARCHAR(20) NOT NULL,
    severitate      enum('nu poate fi testat', 'blocare aplicatie', 
        'esential', 'major', 'mediu', 'minor', 'intrebare', 'sugestie') 
        default 'minor',
    descriere       VARCHAR(30),
    versiunea_ident VARCHAR(10),
    reproductibilitate  VARCHAR(50),
    statutul        enum('neanalizat', 'de nereprodus', 'nu este defect', 
            'nu va fi corectat', 'independent de programator', 'corectat',
            'trebuie corectat') default 'neanalizat',
    rezultatul      enum('defect nou', 'defect verificat', 'defect necorectat')
            default 'defect nou',
    ultima_modif_data    DATE NOT NULL,
    ultima_modif_utiliz BIGINT(13) UNSIGNED NOT NULL,
    FOREIGN KEY (ultima_modif_utiliz) REFERENCES utilizatori (CNP) ON UPDATE CASCADE ON DELETE CASCADE
);

ALTER TABLE bugs ADD CONSTRAINT severitate_chk CHECK (severitate IN ('nu poate fi testat', 'blocare aplicatie', 
        'esential', 'major', 'mediu', 'minor', 'intrebare', 'sugestie'));
ALTER TABLE bugs ADD CONSTRAINT statutul_chk CHECK (statutul IN ('neanalizat', 'de nereprodus', 'nu este defect', 
            'nu va fi corectat', 'independent de programator', 'corectat',
            'trebuie corectat'));
ALTER TABLE bugs ADD CONSTRAINT rezultatul_chk CHECK (rezultatul IN ('defect nou', 
            'defect verificat', 'defect necorectat'));

DELIMITER //
CREATE FUNCTION sever (
	severitate VARCHAR(20)
)
RETURNS INT(2)
BEGIN
	DECLARE result INT(2);
	if strcmp(severitate,'sugestie') = 0 then set result = 8;
	end if;
	if strcmp(severitate,'intrebare') = 0 then set result = 7;
	end if;
	if strcmp(severitate,'minor') = 0
		then
			set result = 6;
	end if;
	if strcmp(severitate,'mediu') = 0
		then
			set result = 5;
	end if;
	if strcmp(severitate,'major') = 0
		then
			set result = 4;
	end if;
	if strcmp(severitate,'esential') = 0
		then
			set result = 3;
	end if;
	if strcmp(severitate,'blocare aplicatie') = 0
		then
			set result = 2;
	end if;
	if strcmp(severitate,'nu poate fi testat') = 0
		then
			set result = 1;
	end if;	
	RETURN result;
END; //

insert into bugs (id_bug, denumire_bug, severitate, descriere,
    versiunea_ident, reproductibilitate, statutul, rezultatul, 
    ultima_modif_data, ultima_modif_utiliz) values 
    (default, 'no output', 'major', 'functia X nu intoarce nimic', '1.0.2', 'usor de reprodus',
    'trebuie corectat', default, '2013-06-12', '1860926240323');

insert into bugs (id_bug, denumire_bug, severitate, descriere,
    versiunea_ident, reproductibilitate, statutul, rezultatul, 
    ultima_modif_data, ultima_modif_utiliz) values 
    (default, 'nothing is happening', 'major', 'daca apas butonul X nu se intampla nimic',
    '1.0.2', 'usor de reprodus',
    'trebuie corectat', default, '2013-07-12', '1860926240323');

insert into bugs (id_bug, denumire_bug, severitate, descriere,
    versiunea_ident, reproductibilitate, statutul, rezultatul, 
    ultima_modif_data, ultima_modif_utiliz) values 
    (default, 'programul crapa', 'esential', 'pentru mai mult de 1000 de intrari crapa',
    '1.0.2', 'trebuie introduse > 1000 input',
    'corectat', default, '2013-06-14','1220713379378');




CREATE TABLE asociere_bug_proiect (
	id_asoc         INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
	id_bug          INT(10) UNSIGNED NOT NULL,
    id_proiect      INT(10) UNSIGNED NOT NULL,
    FOREIGN KEY (id_bug) REFERENCES bugs (id_bug) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (id_proiect) REFERENCES proiect (id_proiect) ON UPDATE CASCADE ON DELETE CASCADE
);

insert into asociere_bug_proiect 
    (id_asoc, id_bug, id_proiect) values
    (default, '1', '1');
insert into asociere_bug_proiect 
    (id_asoc, id_bug, id_proiect) values
    (default, '2', '1');
insert into asociere_bug_proiect 
    (id_asoc, id_bug, id_proiect) values
    (default, '3', '2');

CREATE TABLE comentarii_bug (
	id_comm	    INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
	id_bug  	INT(10) UNSIGNED NOT NULL,
    comentariu  VARCHAR(100) NOT NULL,
    FOREIGN KEY (id_bug) REFERENCES bugs (id_bug) ON UPDATE CASCADE ON DELETE CASCADE    
);

insert into comentarii_bug
    (id_comm, id_bug, comentariu) values
    (default, '1', 'Acest bug este foarte grav si trebuie reparat');

insert into comentarii_bug
    (id_comm, id_bug, comentariu) values
    (default, '1', 'Bug ul este asignat echipei 1');

insert into comentarii_bug
    (id_comm, id_bug, comentariu) values
    (default, '2', 'Acest bug este foarte grav si trebuie reparat');

insert into comentarii_bug
    (id_comm, id_bug, comentariu) values
    (default, '3', 'Acest bug este foarte grav si trebuie reparat');

create table salarii (
    id_intrare          INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
    id_utilizator       BIGINT(13) UNSIGNED NOT NULL,
    luna               	DATE,
    suma                INT(10) UNSIGNED NOT NULL,
    FOREIGN KEY (id_utilizator) REFERENCES utilizatori (CNP) ON UPDATE CASCADE ON DELETE CASCADE
);

insert into salarii (id_intrare, id_utilizator, luna, suma) values (default, '1700713372323', '2012-08-01', '2300');
insert into salarii (id_intrare, id_utilizator, luna, suma) values (default, '1320713372318', '2012-08-01', '2300');

insert into salarii (id_intrare, id_utilizator, luna, suma) values (default, '1220713379378', '2012-08-01', '2300');
insert into salarii (id_intrare, id_utilizator, luna, suma) values (default, '1160121873449', '2012-08-01', '2500');

insert into salarii (id_intrare, id_utilizator, luna, suma) values (default, '1940503835624', '2012-08-01', '2500');
insert into salarii (id_intrare, id_utilizator, luna, suma) values (default, '1560330668692', '2012-08-01', '2800');

insert into salarii (id_intrare, id_utilizator, luna, suma) values (default, '2981007225570', '2012-08-01', '2800');
insert into salarii (id_intrare, id_utilizator, luna, suma) values (default, '1350615180258', '2012-08-01', '1800');
insert into salarii (id_intrare, id_utilizator, luna, suma) values (default, '1860926240323', '2012-08-01', '2000');
insert into salarii (id_intrare, id_utilizator, luna, suma) values (default, '1921202602499', '2012-08-01', '3800');

insert into salarii (id_intrare, id_utilizator, luna, suma) values (default, '1320810148322', '2012-08-01', '1800');
insert into salarii (id_intrare, id_utilizator, luna, suma) values (default, '2390831245242', '2012-08-01', '1100');
insert into salarii (id_intrare, id_utilizator, luna, suma) values (default, '1400515716087', '2012-08-01', '1100');
insert into salarii (id_intrare, id_utilizator, luna, suma) values (default, '2740606757054', '2012-08-01', '4100');
