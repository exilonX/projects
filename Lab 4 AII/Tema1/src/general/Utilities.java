package general;

import java.util.ArrayList;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import databaseconnection.DataBaseConnection;

public class Utilities {
    public static boolean isTableName(String entry) {
        try {
            for (String tableName:DataBaseConnection.getTableNames()) {
                if (entry.toLowerCase().replaceAll(" ","").equals(tableName.toLowerCase().replaceAll("_",""))) {    
                    return true;
                }    
            }
        } catch (Exception exception) {
            System.out.println ("Exceptie: "+exception.getMessage());
        }
        return false;
    }
    
    public static ArrayList<String> select_cols(String[] cols, ArrayList<String> allcols) {
    	ArrayList<String> ret = new ArrayList<>();
    	for (String col : cols) {
    		if (allcols.contains(col)) {
    			ret.add(col);
    		}
    	}
    	return ret;
    }
    
    public static ArrayList<String> select_cols_index(Integer[] cols, ArrayList<String> allcols) {
    	ArrayList<String> ret = new ArrayList<>();
    	for (Integer col : cols) {
    		ret.add(allcols.get(col));
    	}
    	return ret;
    }
    
    public static void add_text(String text, VBox container) {
    	GridPane grid = new GridPane();
    	grid.setAlignment(Pos.CENTER);
    	grid.setHgap(10);
    	grid.setVgap(10);
    	grid.setPadding(new Insets(25, 25, 25, 25));
    	
    	Text scenetitle = new Text(text);
    	scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
    	grid.add(scenetitle, 0, 0, 2, 1);
    	container.getChildren().addAll(scenetitle);
    }
    
    
}
