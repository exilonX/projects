package entities;

import java.util.ArrayList;

import javafx.beans.property.SimpleStringProperty;

public class FacturiProiect extends Entity{
	final private SimpleStringProperty 		denumire;
	final private SimpleStringProperty 		numar_factura;
	final private SimpleStringProperty		data_emitere;
	final private SimpleStringProperty		suma;
	
	public FacturiProiect(String proiect, String nr_factura, String data, String suma) {
		this.denumire = new SimpleStringProperty(proiect);
		this.numar_factura = new SimpleStringProperty(nr_factura);
		this.data_emitere = new SimpleStringProperty(data);
		this.suma = new SimpleStringProperty(suma);
	}
	
	public FacturiProiect(ArrayList<Object> row) {
		System.out.println("This is a row" + row);
		this.denumire = 		new SimpleStringProperty(row.get(0).toString());
		this.numar_factura = 	new SimpleStringProperty(row.get(1).toString());
		this.data_emitere = 	new SimpleStringProperty(row.get(2).toString());
		this.suma = 			new SimpleStringProperty(row.get(3).toString());
	}
	
	public String getDenumire() {
		return denumire.get();
	}

	public String getNumar_factura() {
		return numar_factura.get();
	}

	public String getData_emitere() {
		return data_emitere.get();
	}

	public String getSuma() {
		return suma.get();
	}
	
	@Override
	public ArrayList<String> getValues() {
		ArrayList<String> values = new ArrayList<>();
		values.add(denumire.get());
		values.add(data_emitere.get());
		values.add(numar_factura.get());
		values.add(suma.get());
		return values;
	}
	
}
