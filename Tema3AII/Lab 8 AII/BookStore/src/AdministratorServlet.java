import general.Constants;
import graphicuserinterface.AdministratorGraphicUserInterface;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dataaccess.DataBaseConnection;

public class AdministratorServlet extends HttpServlet {
    final public static long    serialVersionUID = 10011001L;

    private ArrayList<String>   dataBaseStructure;
    private String              selectedTable;
    private String              userDisplayName;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        try {
            DataBaseConnection.openConnection();
            dataBaseStructure   = DataBaseConnection.getTableNames();
            selectedTable       = dataBaseStructure.get(0);
        } catch (SQLException exception) {
            System.out.println("exceptie: "+exception.getMessage());
            if (Constants.DEBUG)
                exception.printStackTrace();
        }
    }

    @Override
    public void destroy() {
        try {
            DataBaseConnection.closeConnection();
        } catch (SQLException exception) {
            System.out.println("exceptie: "+exception.getMessage());
            if (Constants.DEBUG)
                exception.printStackTrace();
        }
    }



    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Enumeration parameters = request.getParameterNames();
        selectedTable = "conturi";
        int operation = Constants.OPERATION_NONE;
        String primaryKeyAttribute = new String();
        try {
            primaryKeyAttribute = DataBaseConnection.getTablePrimaryKey(selectedTable);
        } catch (SQLException exception) {
            System.out.println ("exceptie: "+exception.getMessage());
            if (Constants.DEBUG)
                exception.printStackTrace();
        }
        String primaryKeyValue = new String();
        String id = new String();
        while(parameters.hasMoreElements()) {
            String parameter = (String)parameters.nextElement();
            if (parameter.equals(Constants.LOGOUT)) {
                operation = Constants.OPERATION_LOGOUT;
            }
            if (parameter.contains(Constants.ADD_BUTTON_NAME.toLowerCase())) {
                operation = Constants.OPERATION_INSERT;
                id = parameter.split(" ")[1];
            }
        }

        response.setContentType("text/html");
        try (PrintWriter printWriter = new PrintWriter(response.getWriter())) {
            switch (operation) {
                case Constants.OPERATION_INSERT:
                	ArrayList<String> attribute = new ArrayList<>();
                	attribute.add("activat");
                	ArrayList<String> values = new ArrayList<>();
                	values.add("yes");
                	String whereclause = "id = '"+ id + "'";
					try {
						DataBaseConnection.updateRecordsIntoTable("conturi", attribute, values, whereclause);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
                    break;

                case Constants.OPERATION_LOGOUT:
                	RequestDispatcher requestDispatcher = null;
                	requestDispatcher = getServletContext().getRequestDispatcher("/LoginServlet");
                	if (requestDispatcher != null) {
                		requestDispatcher.forward(request, response);
                	}
                    break;
            }

            HttpSession session = request.getSession(true);
            userDisplayName = session.getAttribute(Constants.IDENTIFIER).toString();
            if (operation == Constants.OPERATION_UPDATE_PHASE1) {
                AdministratorGraphicUserInterface.displayAdministratorGraphicUserInterface(userDisplayName, dataBaseStructure, selectedTable, primaryKeyValue, printWriter);
            }
            else {
                AdministratorGraphicUserInterface.displayAdministratorGraphicUserInterface(userDisplayName, dataBaseStructure, selectedTable, null, printWriter);
            }
        }
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(true);
        userDisplayName = session.getAttribute(Constants.IDENTIFIER).toString();
        response.setContentType("text/html");
        try (PrintWriter printWriter = new PrintWriter(response.getWriter())) {
            AdministratorGraphicUserInterface.displayAdministratorGraphicUserInterface(userDisplayName, dataBaseStructure, selectedTable, null, printWriter);
        }
    }
}
