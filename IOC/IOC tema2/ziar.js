function ZiarItem(src, topMargin, leftMargin, parent, id, numeziar) {
	this.src = src;
	this.topMargin = topMargin;
	this.leftMargin = leftMargin;
	this.parent = parent;
	this.id = id;
	
	var nr_id = id.substring(3, id.length );
	
	this.addImage = function() {
		this.image = document.createElement("img");
		this.image.src = this.src;
		this.image.className = "img";
		this.image.id = "img" + nr_id;
		this.image.setAttribute( "onClick", "javascript: clickdiv(this.id);" );
	}

	this.adddivNume = function() {
		this.divnume = document.createElement("div");
		this.divnume.className = "divnume";
		this.divnume.innerHTML = numeziar;
	}


	this.addButtonSelect = function () {
		this.buttonSelect = document.createElement("a");
		this.buttonSelect.className = "buttonAdauga";
		this.buttonSelect.style.marginTop = "80px";
		this.buttonSelect.id = "select" + nr_id;
		this.buttonSelect.setAttribute( "onClick", "javascript: clickbuttonSelect(this.id);" );
		this.buttonSelect.innerHTML = "Adauga Ziar";
		this.buttonSelect.style.marginLeft = "20px";
	}

	this.addButtonAddOn = function () {
		this.buttonYes = document.createElement("a");
		this.buttonYes.className = "buttonAdauga";
		
		this.buttonYes.id = "byes" + nr_id;
		this.buttonYes.setAttribute( "onClick", "javascript: clickbuttonYes(this.id);" );
		this.buttonYes.innerHTML = "Cu Addon";
		this.buttonYes.style.marginLeft = "0px";
		this.buttonYes.style.marginTop = "0px"
		this.buttonYes.style.visibility = "hidden";
		this.buttonNo = document.createElement("a");
		this.buttonNo.className = "buttonAdauga";
		
		this.buttonNo.id = "bno" + nr_id;
		this.buttonNo.setAttribute( "onClick", "javascript: clickbuttonNo(this.id);" );
		this.buttonNo.innerHTML = "Fara Addon";
		this.buttonNo.style.marginRight = "0px";
		this.buttonNo.style.visibility = "hidden";
	}

	this.addToDocument = function() {
		this.div = document.createElement("div");
		
		this.div.style.left = this.leftMargin;
		this.div.style.top = this.topMargin;
		this.div.style.marginTop = "10px";
		this.div.className = "ziarItem";
		
		this.div.id = this.id;
		// add a image of the newspaper
		this.addImage();
		this.addButtonSelect();
		this.addButtonAddOn();
		this.adddivNume();
		this.div.appendChild(this.image);
		this.div.appendChild(this.divnume);
		this.div.appendChild(this.buttonSelect);
		this.div.appendChild(this.buttonYes);
		this.div.appendChild(this.buttonNo);
		parent.appendChild(this.div);
	}
	
}

