#pragma once
#ifndef GRID_H
#define GRID_H

#include "dependente\glew\glew.h"
#include "dependente\glm\glm.hpp"
#include "dependente\glm\gtc\type_ptr.hpp"
#include "dependente\glm\gtc\matrix_transform.hpp"
#include "VertexFormat.h"

class Grid
{
public:

	Grid(void);
	Grid(unsigned int *mesh_num_ind, unsigned int *mesh_vao, unsigned int *mesh_ibo,
		unsigned int *mesh_vbo, int dim_x, int dim_y);
	~Grid(void);
	void draw_grid();
	void draw_square(glm::mat4 model_matrix, unsigned int loc_model_matrix, glm::vec3 trans_vector, glm::vec3 scale_vector);
	void square(unsigned int gl_program_shader, float size);
private:
	int dim_x;
	int dim_y;
	unsigned int *mesh_num_ind;
	unsigned int *mesh_vao;
	unsigned int *mesh_vbo;
	unsigned int *mesh_ibo;
	glm::vec3 curent_position;
	
	glm::mat4 tranform_square(glm::mat4 model_matrix, glm::vec3 trans_vector);
	

};

#endif