#include "Grid.h"


Grid::Grid(void)
{
}

Grid::Grid(unsigned int *mesh_num_ind, unsigned int *mesh_vao, unsigned int *mesh_ibo,
		unsigned int *mesh_vbo, int dim_x, int dim_y)
{
	this->dim_x = dim_x;
	this->dim_y = dim_y;
	this->mesh_num_ind = mesh_num_ind;
	this->mesh_ibo = mesh_ibo;
	this->mesh_vbo = mesh_vbo;
	this->mesh_vao = mesh_vao;
}

Grid::~Grid(void)
{
}

void Grid::square(unsigned int gl_program_shader, float size) {
	
	VertexFormat quad[4] = {
		VertexFormat(0.0f, 0.0f, size),
		VertexFormat(size, 0.0f, size),
		VertexFormat(0.0f, 0.0f, 0.0f),
		VertexFormat(size, 0.0f, 0.0f)
	};

	unsigned int quad_index[6] = { 0, 2, 1, 1, 2, 3 };

	glGenVertexArrays(1, mesh_vao);
	glBindVertexArray(*mesh_vao);

	glGenBuffers(1, mesh_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, *mesh_vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(VertexFormat) * 4, &quad[0], GL_STATIC_DRAW);

	glGenBuffers(1, mesh_ibo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *mesh_ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * 6, &quad_index[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*) 0);
	/*glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*) (sizeof(float)*3));*/
}

glm::mat4 Grid::tranform_square(glm::mat4 model_matrix, glm::vec3 trans_vector)
{
	curent_position = curent_position + trans_vector;
	return glm::translate(model_matrix, trans_vector);
}



void Grid::draw_square(glm::mat4 model_matrix, unsigned int loc_model_matrix, glm::vec3 trans_vector, glm::vec3 scale_vector)
{
	glm::mat4 new_matrix = tranform_square(model_matrix, trans_vector);
	curent_position = curent_position * scale_vector;
	glm::mat4 scale_matrix = glm::scale(model_matrix, scale_vector);
	glm::mat4 new_matrix1 = glm::rotate(model_matrix, 0.5f, glm::vec3(0,1,0));
	glUniformMatrix4fv(loc_model_matrix, 1, false, glm::value_ptr(new_matrix * scale_matrix));
	glBindVertexArray(*mesh_vao);
	glDrawElements(GL_TRIANGLES, *mesh_num_ind, GL_UNSIGNED_INT, 0);
}

