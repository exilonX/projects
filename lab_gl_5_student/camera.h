#pragma once
#ifndef CAMERA_H
#define CAMERA_H
#include "dependente\glew\glew.h"
#include "dependente\glm\glm.hpp"
#include "dependente\glm\gtc\type_ptr.hpp"
#include "dependente\glm\gtc\matrix_transform.hpp"


class Camera{
	public:
		Camera();
		~Camera();

		void translate_Forward(float dist);
		void translate_Up(float dist);
		void translate_Right(float dist);

		void rotateFPS_OY(float angle);
		void rotateFPS_OX(float angle);
		void rotateFPS_OZ(float angle);
		void rotateTPS_OY(float angle, float dist_to_interes);
		void rotateTPS_OX(float angle, float dist_to_interes);
		void rotateTPS_OZ(float angle, float dist_to_interes);
		void render(glm::vec3 pos, glm::mat4 *view_matrix);
		void render(glm::vec3 pos, glm::vec3 cent, glm::mat4 *view_matrix);
		void init();
		void render(glm::mat4 *view_matrix);
		glm::vec3 position;
	private:
		glm::vec3 forward;
		glm::vec3 up;
		glm::vec3 right;
		
};

#endif