#version 330

layout(location = 0) in vec3 in_position;		
layout(location = 1) in vec3 in_normal;		

uniform mat4 model_matrix, view_matrix, projection_matrix;
uniform vec3 light_position;
uniform vec3 eye_position;
uniform int material_shininess;
uniform float material_kd;
uniform float material_ks;
uniform vec3 color_vector;

float culoareaAmbientalaGlobala = 0.2;

out float light;
out vec3 vertex_to_fragment_color;

void main(){

	vertex_to_fragment_color = color_vector;
	//TODO
	vec3 world_pos = (model_matrix * vec4(in_position, 1)).xyz;
	vec3 world_normal = normalize(mat3(model_matrix) * in_normal);

	vec3 L = normalize(light_position - world_pos);
	vec3 V = normalize(eye_position - world_pos);
	vec3 H = normalize(L + V);

	int primesteLumina = dot(world_normal, L) > 0 ? 1 : 0;

	float emisiva = 0;
	float ambientala = material_ks * culoareaAmbientalaGlobala;
	float difuza = material_kd * max(dot(world_normal, L), 0);
	float speculara = material_ks * primesteLumina * pow((max(dot(world_normal, H),0)), material_shininess);

	light = emisiva + ambientala + difuza + speculara;

	gl_Position = projection_matrix * view_matrix * model_matrix * vec4(in_position, 1); 
}
