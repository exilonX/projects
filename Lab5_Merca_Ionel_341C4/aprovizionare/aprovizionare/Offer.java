package aprovizionare;

import java.io.Serializable;

public class Offer implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 346492920026055568L;
	private String termen_limita;
	private String cost_estimativ;
	
	public Offer() {
		this.termen_limita = new String();
		this.cost_estimativ = new String();
	}
	
	public Offer(String termen, String cost) {
		this.termen_limita = termen;
		this.cost_estimativ = cost;
	}
	

	public String getTermen_limita() {
		return termen_limita;
	}

	public void setTermen_limita(String termen_limita) {
		this.termen_limita = termen_limita;
	}

	public String getCost_estimativ() {
		return cost_estimativ;
	}

	public void setCost_estimativ(String cost_estimativ) {
		this.cost_estimativ = cost_estimativ;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
}
