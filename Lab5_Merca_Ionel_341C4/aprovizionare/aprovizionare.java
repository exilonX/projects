package aprovizionare;

import java.rmi.Remote;
import java.rmi.RemoteException;


public interface Aprovizionare extends Remote {
	public int setProjectInquiry(String projectName, String projectDescription, Devices[] necesaryEquipment) throws RemoteException;
	public int getProjectInquiryStatus(int projectId) throws RemoteException;
	public Offer getProjectInquiry(int projectId) throws RemoteException;
	public boolean setProjectRequest(int projectId) throws RemoteException;
	public Offer[] getEquipmentInquiry(Devices[] necessaryEquipment) throws RemoteException;
	public boolean setEquipmentRequest(Devices[] necessaryEquipment) throws RemoteException;
}
