Drop DATABASE Grupa341C4_MercaIonel_ProdAsus;

CREATE DATABASE Grupa341C4_MercaIonel_ProdAsus;

USE Grupa341C4_MercaIonel_ProdAsus;


CREATE table modele (
	
	id_model 		INT(2) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
	model 			VARCHAR(20) NOT NULL,
	stoc			INT(5) UNSIGNED NOT NULL,
	pret			INT(5) UNSIGNED NOT NULL,
	timp_livrare	INT(2) UNSIGNED NOT NULL
	);

insert into modele values (default, "p43neof", 100, 200, 2);
insert into modele values (default, "p43neo", 120, 230, 3);
insert into modele values (default, "alfax2", 150, 200,4);
insert into modele values (default, "G7123", 10, 1200, 5);
insert into modele values (default, "J2312", 130, 200, 3);
insert into modele values (default, "G2222", 1000, 10, 2);

insert into modele values (default,"9400gt", 200, 500, 3);
insert into modele values (default,"9600gt", 20, 200, 3);
insert into modele values (default,"9800GT", 100, 50, 4);
insert into modele values (default,"9800gtx", 230, 230, 2);
insert into modele values (default,"GTX260", 200, 100, 1);
insert into modele values (default,"gtx240", 200, 2000, 1);


