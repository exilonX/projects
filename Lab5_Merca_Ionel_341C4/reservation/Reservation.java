package reservation;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;

public interface Reservation extends Remote {
	public ArrayList<Interval> getTimetable() throws RemoteException;
	public int getAvailableSeats(Interval interval) throws RemoteException;
	public boolean makeReservation(int clientId, Interval interval, int numberOfSeats) throws RemoteException;
	public boolean cancelReservation(int clientId, Interval interval) throws RemoteException;
	public HashMap<Interval, Integer> getReservation(int customerId) throws RemoteException;
}
