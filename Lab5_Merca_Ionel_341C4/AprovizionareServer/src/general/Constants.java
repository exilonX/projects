package general;

public interface Constants {
	final public static String      APPLICATION_NAME            = "ERP - Companie Dezvoltare Software";
	final public static String      DATABASE_CONNECTION         = "jdbc:mysql://localhost:3306/Grupa341C4_MercaIonel_Aprovizionare";
	final public static String      DATABASE_NAME               = "Grupa341C4_MercaIonel_Aprovizionare";
	final public static String      DATABASE_USER               = "root";
	final public static String      DATABASE_PASSWORD           = "";
	
	final public static boolean     DEBUG                       = true;
    final public static String      FXML_DOCUMENT_NAME          = "authentication.fxml";
    
    final public static double      SCENE_WIDTH_SCALE           = 0.99;
    final public static double      SCENE_HEITH_SCALE           = 0.90;
    final public static int         DEFAULT_SPACING             = 10;
    final public static int         DEFAULT_GAP                 = 5;
    final public static int         DEFAULT_COMBOBOX_WIDTH      = 125;
    final public static int         DEFAULT_TEXTFIELD_WIDTH     = 10;
    final public static String      SUBMIT_BUTTON               = "Acceptare";
    
	final public static String[]	ENUM_STATUS 				= {"analizata", "neanalizata"};
	final public static String[]	FIELDS						= {"numar_programatori", "nr_qa", "nr_ore", "complexitate"};
}