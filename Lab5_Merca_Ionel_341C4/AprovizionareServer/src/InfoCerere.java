import java.util.ArrayList;


public class InfoCerere {
	private ArrayList<String> programatori;
	private ArrayList<String> qa;
	private int nr_prog;
	private int nr_qa;
	private String date;
	
	public InfoCerere() {
		programatori = new ArrayList<>();
		qa = new ArrayList<>();
		nr_prog = 0;
		nr_qa = 0;
		date = "";
	}
	

	public ArrayList<String> getProgramatori() {
		return programatori;
	}

	public void setProgramatori(ArrayList<String> programatori) {
		this.programatori = programatori;
	}

	public ArrayList<String> getQa() {
		return qa;
	}

	public void setQa(ArrayList<String> qa) {
		this.qa = qa;
	}

	public int getNr_prog() {
		return nr_prog;
	}

	public void setNr_prog(int nr_prog) {
		this.nr_prog = nr_prog;
	}

	public int getNr_qa() {
		return nr_qa;
	}

	public void setNr_qa(int nr_qa) {
		this.nr_qa = nr_qa;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
}
