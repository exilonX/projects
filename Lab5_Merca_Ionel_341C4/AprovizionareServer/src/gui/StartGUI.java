package gui;

import databaseconnection.DataBaseConnection;
import general.Constants;
import gui.CoordonatorGUI;

import java.sql.ResultSet;
import java.util.ArrayList;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class StartGUI extends Application implements EventHandler {  
	private Stage               applicationStage;
	private Scene               applicationScene;

	@FXML private TextField     campTextNumeUtilizator;
	@FXML private PasswordField campTextParola;
	@FXML private Label         etichetaAfisare;    

	public StartGUI() {
		
	}

	@Override
	public void start(Stage mainStage) {
		applicationStage = mainStage;
		try {
			applicationScene = new Scene((Parent)FXMLLoader.load(getClass().getResource(Constants.FXML_DOCUMENT_NAME)));
			applicationScene.addEventHandler(EventType.ROOT,(EventHandler<? super Event>)this);
		} catch (Exception exception) {
			System.out.println ("exception : "+exception.getMessage());
		}
		applicationStage.setTitle(Constants.APPLICATION_NAME);
		applicationStage.setScene(applicationScene);
		applicationStage.show();
	}

	@FXML protected void handleButonAcceptareAction(ActionEvent event) throws Exception {
		ArrayList<String> attributes = new ArrayList<>();
		attributes.add("nume");
		attributes.add("prenume");

		String query = "select nume, prenume from utilizatori where nume_utilizator = " + "\'" 
				+ campTextNumeUtilizator.getText() + "\' and parola = \'" + campTextParola.getText() + "\'";

		
		ArrayList<ArrayList<Object>> result = DataBaseConnection.executeQuery(query);
		
		if (result.size() > 0) {
			ArrayList<Object> r = result.get(0);
			String query1 = "select functia from utilizatori where nume_utilizator = '" + 
					campTextNumeUtilizator.getText() + "'";
			String functie = DataBaseConnection.executeQuery(query1).get(0).get(0).toString();
			if (functie.equals("coordonator")) {
				System.out.println("Bine ai venit " + r.get(0) + " " + r.get(1));
				CoordonatorGUI coordonator = new CoordonatorGUI();
				coordonator.start();
				coordonator.handle(null);
			} else {
				etichetaAfisare.setText("Doar coordonatori se pot loga");
			}
		}
		else {
			etichetaAfisare.setText("Userul si parola nu sunt corecte!!!");
		}
	}

	@FXML protected void handleButonRenuntareAction(ActionEvent event) {
		System.exit(0);
	}

	@Override
	public void handle(Event event) {
		if (event.getTarget() instanceof Button && ((Button)event.getTarget()).getText().equals(Constants.SUBMIT_BUTTON) && event.getEventType().equals(ActionEvent.ACTION) && !applicationStage.isFocused()) {
			applicationStage.hide(); 
		}
	}
	

	public static void main(String[] args) {
		launch(args);
	}
}
