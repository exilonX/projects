% get a random element from a list
rand_elem([], _) :- fail.
rand_elem(List, Elt) :-
	length(List, Len),
	random(0, Len, Index),
	nth0(Index, List, Elt).

%generate a random list of portals
% rand_next_portals(+Lista, +Rand, -Acc, +Nr, +Curent)
rand_next_portals([], [], [], _, _).
rand_next_portals(_, A, A, 0, _).
rand_next_portals(Lista, Rand, Acc, Nr, Curent) :-
	rand_elem(Lista, X),
        Nr > 0,
	(not( member(X, Rand)), X \= Curent,
	Nr1 is Nr - 1,
	rand_next_portals(Lista, [X | Rand], Acc, Nr1, Curent), !;
	rand_next_portals(Lista, Rand, Acc, Nr, Curent)).

% generate a random list of portals
% rand_portals(+Lista, +Curent, -Rand)
rand_portals(Lista, Curent, Rand) :-
	length(Lista, Len),
	Len1 is Len,
	random(1, Len1, Nr),
	rand_next_portals(Lista, [], Rand, Nr, Curent).


% functie ce returneaza starea/locul initial dintr-o lista de stari
% getInit(+ListaLocuri, -Initial)
getinit([], _) :- fail.
getinit([[A, B, initial] | _], X) :-
	X = [A, B, initial], !.
getinit([_ | T], X) :-
	getinit(T, X).

% return the coordinates of the gate
getgate([], _) :- fail.
getgate([[A, B, gate] | _], X) :-
	X = [A, B, gate], !.
getgate([_ | T], X) :-
	getinit(T, X).


%verificare daca acest loc este locul initial
initiala([_, _, initial]).

%verificare daca acest loc este locul cu poarta
gate([_, _, gate]).

%verificare daca locul contine energie
energy([_, _, energy]).

/*
energy_of_portal([XI,YI,_], [XN, YN,_], CurentEn, NextE) :-
*/

emptyq([]).
% enqueue(+El, +Q, -QNoua) introduce un element in coada
enqueue(El, [], [El]).
enqueue(El, [X | Rest], [X | R1]) :- enqueue(El, Rest, R1).
% dequeue(-El, +Q, -QNoua) elimina un element din coada
dequeue(El, [El | R], R).


inspq(El, [], [El]).
inspq(El, [X | Rest], [El, X | Rest]) :- precedes(El, X), !.
inspq(El, [X | Rest], [X | R1]) :- inspq(El, Rest, R1).
precedes([_, _, _, _, F1], [_, _, _, _, F2]) :- F1<F2.

% find a key in a map
% find_key(+Key, +Map, -Value)
find_key(_, [], []).
find_key(Key,[elem(Key, Values) | _], Values) :- !.
find_key(Key, [_ | Rest], Values) :-
	find_key(Key, Rest, Values).

% eliminate a specific element from a list
% elim(+ToDelete, +List, -Result)
elim(_, [], []).
elim(X, [X | Rest], Rest) :- !.
elim(X, [Y | Rest], [Y | Rest1]) :- elim(X, Rest, Rest1).

% eliminate if the coordinates are the same
% elim same coords
elimSC(_, [], []).
elimSC([X, Y, _], [[X, Y, _] | Rest], Rest) :- !.
%elimSC([X, Y], [[X, Y, _] | Rest], Rest) :- !.
elimSC([X, Y, _], [[X, Y] | Rest], Rest) :- !.
elimSC([X, Y], [[X,Y] | Rest], Rest) :- !.
elimSC(X, [Y | Rest], [Y | Rest1]) :- elimSC(X, Rest, Rest1).


%check if a list is empty
empty([]).

%make a map datastructure to store the list of portals for each place
%insert_map(+Key, +Value, +Map, -MapResult)
%insert into a empty map
insert_map(Key, Value, [], [elem(Key, [Value])]) :- !.

%insert into a map that already contains that key
insert_map(Key, Value, Map, MapR) :-
	find_key(Key, Map, Values),
	not(empty(Values)),!,
	elim(elem(Key, Values), Map, Map1),
	enqueue(Value, Values, ValuesR),
	enqueue(elem(Key, ValuesR), Map1, MapR).

% insert into a not empty map that doesn't contain that key
insert_map(Key, Value, Map, MapR) :-
	find_key(Key, Map, Values),
	empty(Values),
	enqueue(Value, [], Val),
	enqueue(elem(Key, Val), Map, MapR).

print_map([]) :- nl.
print_map([elem(Key, Values) | Rest]) :-
	write('Key: '), write(Key), write(' Values : '), write(Values), nl,
	print_map(Rest).

% function that gets the coordinates X and Y from both a list with 3 and
% 2 arguments get_coords(+Lista, -X, -Y).
get_coords([X, Y, _], X, Y).
get_coords([X, Y], X, Y).

%compute the manhattan distance between 2 points in a 2d space
% manhattan(+L1, +L2, -Dist)
manhattan(L1, L2, Dist) :-
	get_coords(L1, XL1, YL1),
	get_coords(L2, XL2, YL2),
	abs((XL1 - XL2), DX),
	abs((YL1 - YL2), DY),
	Dist is DX + DY.

% get the list of all the neighbours at 3 * Se distance
% neighbours_3se(+StareCurenta, +ListaLocuri, +Se, -VeciniApropiati)
neighbours_3se(_, [], _ , []).
neighbours_3se(Stare, [X | Locuri], Se, [X | Negh]) :-
	manhattan(Stare, X, D),
	D =< (3 * Se),!,
	neighbours_3se(Stare, Locuri, Se, Negh).
neighbours_3se(Stare, [X | Locuri], Se, Negh) :-
	manhattan(Stare, X, D),
	not(D =< (3 * Se)),
	neighbours_3se(Stare, Locuri, Se, Negh).


% select a random number of random neighbours from a list of neighbours
% that have the manhattan distance less than 3 * Se from Stare
best_random_neighbours(Stare, Locuri, Se, Negh) :-
	neighbours_3se(Stare, Locuri, Se, Neigh),
	write(Neigh), nl,
	rand_portals(Neigh, Stare, Negh).

% delete all elements that are in List2 from List1
% delete_all(+List1, +List2, -Res).
delete_all(A, [], A).
delete_all(List1, [H | List2], Rest) :-
	elimSC(H, List1, Res1),
	delete_all(Res1, List2, Rest).


% when generating random portals from a place you have to take into
% account the portals that are already so you have to delete from the
% list of portals the portals that are already and after that generate
% the best portals
% my_next_neighbours(+InitialState, +AllthePlaces, +Se, +Map, -Neigh)
my_next_neighbours(Stare, Locuri, Se, Map, Neigh) :-
	find_key(Stare, Map, Values),
%	write(Values), nl,
	delete_all(Locuri, Values, NewLoc),
%	write(NewLoc), nl,
	elimSC(Stare, NewLoc, New1),
%	write(New1), nl,
	best_random_neighbours(Stare, New1, Se, Neigh).



% insert into a map where the key is a state and the value is a list of
% adiacent states but update both ends of the portal
insert_map_all(_, [], Map, Map).
insert_map_all(State, Neigh, Map, MapR) :-
	insert_first_end(State, Neigh, Map, MapR1),
	insert_second_end(State, Neigh, MapR1, MapR).

% second end portal insert
% insert_second_end(+State, +Neigh, +Map, -MapR).
insert_second_end(_, [], Map, Map).
insert_second_end(State, [First | Rest], Map, MapR) :-
	insert_map(First, State, Map, MapR1),
	insert_second_end(State, Rest, MapR1, MapR).

% insert first end of portal
% insert_first_end(+State, +Neigh, +Map, -MapR).
insert_first_end(_, [], Map, Map).
insert_first_end(State, [First | Rest], Map, MapR) :-
	insert_map(State, First, Map, MapR1),
	insert_first_end(State, Rest, MapR1, MapR).

% function that gets the coordinates of all the energies
% get_all_energy(+Locuri, -EnergyPlaces)
get_all_energy([], []).
get_all_energy([H | Tail], [H | Rest]) :-
	energy(H),!,
	get_all_energy(Tail, Rest).
get_all_energy([H | Tail], Rest) :-
	not(energy(H)),
	get_all_energy(Tail, Rest).

% get_energy_places(+State, +EnergyPlaces, +InitialSignal, -MyEnergy).
get_energy_places(_, [], _,[]).
get_energy_places(State, [H | T], Se, [H | My]) :-
	manhattan(State, H, Dist),
	Dist =< Se,
	get_energy_places(State, T, Se, My).
get_energy_places(State, [H | T], Se, My) :-
	manhattan(State, H, Dist),
	not(Dist =< Se),
	get_energy_places(State, T, Se, My).


% function that gets all the energy places's signal that gets to
% a specific place
% get_my_energy(+State, +Locuri, +InitSignal, -MyEnergy).
get_my_energy(State, Locuri, Se, My) :-
	get_all_energy(Locuri, Eng),
	get_energy_places(State, Eng, Se, My).


% get a list of signals from the energy points near State
% list_signals(+State, +NearEnergyPlaces, +Acumulator, -List)
list_signals(_, [], _, A, A).
list_signals(State, [H | En], Se, Acc, List) :-
	manhattan(State, H, Dist),
	Dist1 is (Se - Dist),
	list_signals(State, En, Se, [Dist1 | Acc], List).

% call list of signals gets a list with all the signals first
% it gets all the near energy points and then it computes the
% manhattan distance to each point
call_list_signals(State, Locuri, Se, Lista) :-
	get_my_energy(State, Locuri, Se, My),
	list_signals(State, My, Se, [], Lista).

% the maximum value in a list
% max(+List, +Acc(small value), -Max).
max([], Max, Max).
max([H | Tail], Acc, Max) :-
	(Acc < H, max(Tail, H, Max);
	Acc >= H, max(Tail, Acc, Max)).


% the signal have the same freq so the signals can overlap the result
% is the maximum signal
% max_signal(+State, +Locuri, +Se, -Maximum signal)
max_signal(State, Locuri, Se, Max) :-
	call_list_signals(State, Locuri, Se, Lista),
	max(Lista, -1000, Max).

% return the signal received from the gate
gate_signal(State, Locuri, Pint, Max) :-
	getgate(Locuri, Gate),
	manhattan(Gate, State, Dist),
	Max is (Pint - Dist).

% take energy - if the player gets to a place in which there is a energy
% then eliminate that energy place from the list of places
% take_energy(+State, +Locuri, -NewLocuri)
take_energy(State, Locuri, NewL) :-
	elim(State, Locuri, New1),
	get_coords(State, X, Y),
	enqueue(X, [], A),
	enqueue(Y, A, NewState),
	enqueue(NewState, New1, NewL).


% functie care calculeaza euristica pentru calea de la St la L
% euristic(+St, +AllThePlaces, +MyCurentEnergy, pachete(Inten, Quant),
% poarta(Intens, ReqEng), -Val)

%starea urmatoare este o poarta si nu am energia necesara
euristic(State, _, MyEn, _, poarta(Pint, Etot), Val) :-
	gate(State),!,
	MyEn < Etot,
	Val = (-100 * Pint).

% starea urmatoare contine o poarta si am energia necesara
euristic(State, _, MyEn, _, poarta(Pint, Etot), Val) :-
	gate(State),!,
	MyEn >= Etot,
	Val = 100 * Pint.

% am energia necesara pentru a trece prin poarta deci calculez totul in
% functie de poarta
euristic(State, Locuri, MyEn, _, poarta(Pint, Etot), Val) :-
	MyEn >= Etot,!,
	gate_signal(State, Locuri, Pint, Val).

% starea urmatoarea contine o energie
euristic(State, Locuri, MyEn, pachete(Eint, _), poarta(_, Etot), Val) :-
	MyEn < Etot,
	energy(State),!,
	Val1 is (5 * Eint),
	max_signal(State, Locuri, Eint, Max),
	Val is (Val1 + Max).

% starea urmatoare nu contine energie si am nevoie de energii
euristic(State, Locuri, MyEn, pachete(Eint, _), poarta(_, Etot), Val) :-
	MyEn < Etot,
	max_signal(State, Locuri, Eint, Val).

% apply the euristic function on all the neighbours of a specific state,
% store the values returned in a list
% apply_euristic(+Neighbours, +AllPlaces, +MyEnergy, +Pachete, +Poarta,
% +AccumList, -ListofValues)
apply_euristic([], _, _, _, _, L, L).
apply_euristic([H | ListN],Locuri, MyEn, Pachete, Poarta, ListAcc, ListVal) :-
	euristic(H, Locuri, MyEn, Pachete, Poarta, Val),
	apply_euristic(ListN, Locuri, MyEn, Pachete, Poarta, [Val | ListAcc], ListVal).

% get the best way from a list of neighbours
% get_best_path()
get_best_path(Neigh, Locuri, MyEn, Pachete, Poarta, Val) :-
	apply_euristic(Neigh, Locuri, MyEn, Pachete, Poarta, [], ListVal),
	max(ListVal, -1000, Max),
	indexof(Index, Max, ListVal),
	length(Neigh, Len),
	Index1 is (Len - Index + 1),
%	write('Neigh '), write(Neigh), nl,
	nth1(Index1, Neigh, Val).

%get index of a elemnt is a list
indexof(Index, Item, List):-
	nth1(Index, List, Item).
indexof(-1, _, _).

% TODO sa retin alea pe care le-am extins in ceva
% TODO sa updatez alea din lista de vecini

update_elem(Elem, [], Elem).
update_elem([X, Y, E], [[X, Y, E] | _], [X, Y, E]) :- !.
update_elem([X, Y, _], [[X, Y] | _], [X, Y]) :- !.
update_elem([X, Y], [[X, Y] | _], [X, Y]) :- !.
update_elem(X, [Y | Tail], Elem) :-
	update_elem(X, Tail, Elem).



update_list([], _, N, N).
update_list([H | ToUpdate], Updated, Acc, New) :-
	member(H, Updated),
	update_list(ToUpdate, Updated, [H | Acc], New).


update_list([H | ToUpdate], Updated, Acc, New) :-
	not(member(H, Updated)),
	update_elem(H, Updated, Elem),
	update_list(ToUpdate, Updated, [Elem | Acc], New).



% if the curent path contains a energy
find_path(State, problema(Locuri, pachete(Eints, ECant), poarta(PInts, Etot)),
	  MapVecini, Solutie, TotalEnergy, ListExp) :-
	energy(State),!,
	TotalEnergy1 is (TotalEnergy + ECant),
	take_energy(State, Locuri, NewLocuri),
	update_list(ListExp, NewLocuri,[], NewListExp),
	(   not(member(State, NewListExp)),
	    my_next_neighbours(State, Locuri, Eints, MapVecini, Neigh),
	    write('Neighbours '), write(Neigh), nl,
	    insert_map_all(State, Neigh, MapVecini, MapR),
	    enqueue(State, NewListExp, ListExp1),
	    enqueue(State,Solutie, Solutie1),
	    get_best_path(Neigh, NewLocuri, TotalEnergy1, pachete(Eints, ECant),
			  poarta(PInts, Etot), BestNeigh),
	    find_path(BestNeigh, problema(NewLocuri, pachete(Eints, ECant),
				      poarta(PInts, Etot)), MapR, Solutie1, TotalEnergy1, ListExp1)
	;
	    member(State, NewListExp),
	    find_key(State, MapVecini, Values),
	    update_list(Values, NewLocuri, [], NewValues),
	    enqueue(State,Solutie, Solutie1),
	    get_best_path(NewValues, NewLocuri, TotalEnergy, pachete(Eints, ECant),
			  poarta(PInts, Etot), BestNeigh),
	    find_path(BestNeigh, problema(NewLocuri, pachete(Eints, ECant),
				      poarta(PInts, Etot)), MapR, Solutie1, TotalEnergy1, NewListExp)
	).


find_path(State, problema(Locuri, pachete(Eints, ECant), poarta(PInts, Etot)),
	  MapVecini, Solutie, TotalEnergy, ListExp) :-
	(   not(member(State, ListExp)),
	    my_next_neighbours(State, Locuri, Eints, MapVecini, Neigh),
	    write('Neighbours '), write(Neigh), nl,
	    insert_map_all(State, Neigh, MapVecini, MapR),
	    enqueue(State, ListExp, ListExp1),
	    enqueue(State,Solutie, Solutie1),
	    get_best_path(Neigh, NewLocuri, TotalEnergy, pachete(Eints, ECant),
			  poarta(PInts, Etot), BestNeigh),
	    find_path(BestNeigh, problema(NewLocuri, pachete(Eints, ECant),
				      poarta(PInts, Etot)), MapR, Solutie1, TotalEnergy, ListExp1)
	;
	    member(State, ListExp),
	    find_key(State, MapVecini, Values),
	    update_list(Values, Locuri, [], NewValues),
	    enqueue(State,Solutie, Solutie1),
	    get_best_path(NewValues, Locuri, TotalEnergy, pachete(Eints, ECant),
			  poarta(PInts, Etot), BestNeigh),
	    find_path(BestNeigh, problema(Locuri, pachete(Eints, ECant),
				      poarta(PInts, Etot)), MapR, Solutie1, TotalEnergy, ListExp)
	).

find_path(State, problema(_, _, poarta(_, Etot)), _, _, TotalEnergy, _) :-
	gate(State),
	TotalEnergy < Etot,!,
	write('Game Over'), nl.

find_path(State, problema(_, _, poarta(_, Etot)), _, _, TotalEnergy, _)	:-
	gate(State),
	TotalEnergy >= Etot, !,
	write('Congratulations, Game finished'), nl.






find_path_init(problema(Locuri, pachete(Eints, ECant), Poarta), MapVecini,
	       Init, Solutie) :-
	my_next_neighbours(Init, Locuri, Eints, MapVecini, Neigh),
	write('Neighbours '), write(Neigh), nl,
	insert_map_all(Init, Neigh, MapVecini, MapR),
	enqueue(Init, [], ListExp),
	enqueue(Init, [], Solutie),
	get_best_path(Neigh, Locuri, 0, pachete(Eints, ECant), Poarta, BestNeigh),
	find_path(BestNeigh, problema(Locuri, pachete(Eints, ECant), Poarta), MapR, Solutie, 0, ListExp).



go(problema(Locuri, pachete(EInts, ECant), poarta(PInts, Etot)), Solutie) :-
	getinit(Locuri, Init),
	find_path_init(problema(Locuri, pachete(EInts, ECant),
			   poarta(PInts, Etot)), [], Init, Solutie).





