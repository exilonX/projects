% 1. (1p)
% Sa se completeze implementarea pentru problema celor 8 regine
% inceputa in capitolul "Backtracking atunci c�nd cunoa�tem 
% lungimea c�ii c�tre solu�ie" din documentatie.

template([1/Y1,2/Y2,3/Y3,4/Y4,5/Y5,6/Y6,7/Y7,8/Y8]).
solve(S) :- template(S), solution(S).

start([]). 
solution(S) :- start(S). 
solution([X/Y|Others]) :- 
	solution(Others),
	member(Y,[1,2,3,4,5,6,7,8]),  
	safe(X/Y,Others). 
	
% check1 :- aggregate_all(count, solve(_), Count), Count = 92.
