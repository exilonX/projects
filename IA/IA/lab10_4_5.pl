% 4. (3p)
% Sa se implementeze predicatele generice bfs si get_path, ambele
% descrise  in capitolul "C�utare �n l��ime" din documentatie.
% Pentru testare, determinati drumul minim intre nodurile a si g in 
% urmatorul graf:
% start(a).
% succ(a,b). succ(b,c). succ(c,d). succ(d,g). succ(a,e). succ(e,f). succ(f,g).
% goal(g).

% ____________HINT: folositi predicatul findall______________

solve(S) :- 
	start(Start), 
	bfs([[Start,nil]],[],Q),
	get_path(Q,P),
	reverse(P,S).

start(a).
succ(a,b). succ(b,c). succ(c,d). succ(d,g). succ(a,e). succ(e,f). succ(f,g).
goal(g).

% check4 :- solve(S), S = [a,e,f,g].

% Bonus 5. (2p)
% Implementati un mecanism generic pentru cautarea in adancime (dfs).

% check5 :- solve(S), S = [a,b,c,d,g].

