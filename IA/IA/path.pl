emptyq([]).

enqueue(El, [], [El]).
enqueue(El, [X | R], [X | R1]) :- enqueue(El, R, R1).

dequeue(El, [El | R], R).

succesor(a, b). succesor(b, c). succesor(c, d). succesor(d, g).
succesor(a, e). succesor(e, f). succesor(f, g).
final(g).

euristic(a, g, 3).
euristic(b, g, 3).
euristic(c, g, 2).
euristic(d, g, 1).
euristic(g, g, 0).
euristic(e, g, 2).
euristic(f, g, 1).
euristic(_, _, 0).

inspq(El, [], [El]).
inspq(El, [X | _], [El, X | _]) :- precedes(El, X), !.
inspq(El, [X | Rest], [X | R]) :- inspq(El, Rest, R).
precedes([_, _, _, _, F1], [_, _, _, _, F2]) :- F1 < F2.

rezastar(Si, Scop) :-
	emptyq(Frontiera), emptyq(Teritoriu),
	euristic(Si, Scop, H),
	inspq([Si, nil, 0, H, H], Frontiera, Frontiera1),
	astar(Frontiera1, Teritoriu, Scop).

astar(Frontiera, _, _) :-
	emptyq(Frontiera), !, write('Nu exista solutie'), nl.
astar(Frontiera, Teritoriu, Scop) :-
	dequeue([S, Pred, _, _, _], Frontiera, _),
	S = Scop,
	write('S-a gasit solutie'), nl,
	scriecale1([S, Pred, _, _, _], Teritoriu).
astar(Frontiera, Teritoriu, Scop) :-
        dequeue([S, Pred, G, H, F], Frontiera, RestF),
	inspq([S, Pred, G, H, F], Teritoriu, Teritoriu1),
	(bagof([Urmator, H1], (succesor(S, Urmator), euristic(Urmator, Scop, H1)), LSucc),
	!, G1 is G + 1,
	actual_toti(S, G1, LSucc, RestF, Teritoriu1, FrontieraR, TeritoriuR) ;
	FrontieraR = RestF, TeritoriuR = Teritoriu1),
	astar(FrontieraR, TeritoriuR, Scop).

actual_toti(_, _, [], Frontiera, Teritoriu, Frontiera, Teritoriu) :- !.

actual_toti(Stare, G, [[S, H] | Rest], Frontiera, Teritoriu, FrontieraR, TeritoriuR) :-
	actual(Stare, G, [S, H], Frontiera, Teritoriu, Frontiera1, Teritoriu1),
	actual_toti(Stare, G, Rest, Frontiera1, Teritoriu1, FrontieraR, TeritoriuR).

actual(Stare, G, [S, H], Frontiera, Teritoriu, FrontieraR, TeritoriuR) :-
	member([S, Pred, G1, _, _], Teritoriu), !,
	(G1 =< G, TeritoriuR = Teritoriu, FrontieraR=Frontiera, !;
	F is G + H, elim([S, Pred, G1, _, _], Teritoriu, TeritoriuR),
	inspq([S, Stare, G, H, F], Frontiera, FrontieraR)).
actual(Stare, G, [S, H], Frontiera, Teritoriu, FrontieraR, Teritoriu) :-
	F is G + H,
	inspq([S, Stare, G, H, F], Frontiera, FrontieraR).


scriecale1([S, nil, _, _, _], _) :- scrie(S), nl.
scriecale1([S, Pred, _, _, _], Teritoriu) :-
	member([Pred, P, _, _, _], Teritoriu),
	scriecale1([Pred, P, _, _, _], Teritoriu), scrie(S), nl.
scrie(S) :- write(S).

elim(_, [], []).
elim(X, [X | Rest], Rest) :- !.
elim(X, [Y | Rest], [Y | Rest1]) :- elim(X, Rest, Rest1).









