% 2. (3p)
% Sa se implementeze predicatul generic search descris in capitolul
% "Backtracking atunci c�nd calea c�tre solu�ie admite un num�r 
% nedeterminat de st�ri intermediare". Abstractizati procesul de 
% cautare, urmand a obtine ulterior cu minim de efort solutii pentru 
% probleme diverse.

solve(S) :-
      start(Start),
      search(Start,[],Q),
      reverse(Q,S).
	  
% search(+StareCurenta,+StariVizitate,-Solutie)

% 3. (3p)
% Rezolvati problema cu taranul, lupul, capra si varza explicitand
% predicatele generice din mecanismul de cautare implementat anterior.
% Enunt: Un taran isi plimba lupul, capra si varza prin lume, avand 
% mare grija de ele. Lupul tanjeste la o friptura de capra, in timp 
% ce capra isi face planuri de dieta pe seama verzei. Intr-un punct al
% calatoriei taranul este nevoit sa traverseze un rau impreuna cu zestrea
% sa, insa podul precar ii permite sa treaca raul doar singur sau impreuna
% cu unul singur din bunurile sale. Cum trebuie sa se plimbe taranul 
% inainte si inapoi pe pod astfel incat sa-si stramute avutul intact pe 
% partea cealalta?


