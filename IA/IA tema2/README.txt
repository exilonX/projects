Merca Ionel 341 C4

Prima faza in rezolvare a fost parcurgerea de la rezultat adica din varful arborelui la fapte adica frunzele arborelui.
Pentru a face asta am implementat o functie care pentru un anumit atribut parcurge toate regulile si cauta toate atributele care sunt copii acelui atribut adica acele atribute care sunt intr-o conditie si duc la atributul pe care vrem sa il cautam. Plecand de la rezultat am mers in jos si am cautat acele atribute care au influenta pentru rezultatul final formand o coada.
Functia get_all_scopuri returneaza lista de scopuri care trebuiesc cautate in fapte sau in memoria de lucru.
Apoi dupa ce am obtinut lista de scopuri caut sa vad daca sunt satisfacute acele scopuri.
Parcurg lista de scopuri si pentru fiecare atribut verific daca este in fapte daca da atunci adaug in memoria de lucru acel fapt iar daca nu atunci caut in reguli daca exista conditii care sutn satisfacute (adica dau match pe memoria de lucru) si care au in concluzie acel fapt. Adaug toate concluziile care au dat match intr-o lista auxiliara, aplic regulile 1 2 3, iar daca atributul este monovaloare aleg concluzia cu cel mai mare cf. Daca este multivaloare adaug toate concluziile in memoria de lucru. Fac asta pana cand lista de scopuri este nula adica pana cand se ajunge la rezultat rezultatul fiind ultimul element din coada.
Pentru citirea din fisier am parcurs linie cu linie, am transformat in lista de stringuri fiecare linie si am concatenat fiecare lista pentru fiecare regula, apoi am parsat regula cu totul.
atributele - best-* le-am considerat monovalued pentru ca doar o chestie poate sa fie buna
