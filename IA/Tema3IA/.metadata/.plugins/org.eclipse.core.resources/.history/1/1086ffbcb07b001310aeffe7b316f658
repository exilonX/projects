import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;

import javax.crypto.spec.IvParameterSpec;
import javax.swing.text.StyleContext.SmallAttributeSet;


public class ExplorerRobot {
	private char[][] worldmap;
	private char[][] robotmap;
	private int n,m;
	private HashSet<Position> secure;
	private HashSet<Position> unsecure;
	private HashSet<Position> walls;
	private HashSet<Position> swamps;
	private LinkedList<Position> curentRoad;
	
	public ExplorerRobot(char[][] worldmap) {
		this.worldmap = worldmap;
		n = worldmap.length;
		m = worldmap[0].length;
		
		this.secure = new HashSet();
		this.unsecure = new HashSet();
		this.walls = new HashSet();
		this.swamps = new HashSet();
		this.curentRoad = new LinkedList<Position>();
	}
	
	
	/**
	 * Find the initial point from which the robot starts
	 * @return
	 */
	public Position findinit() {
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				if (worldmap[i][j] == 'i') {
					return new Position(j, i);
				}
			}
		}
		return null;
	}
	
	/**
	 * Next move to the north
	 * @param curent
	 * @return
	 */
	public Position moveN(Position curent) {
		int x = curent.getX();
		int y = curent.getY() + 1;
		
		if (y >= n) {
			return null;
		}
		return new Position(x, y);
	}
	
	/**
	 * Next move to the South
	 * @param curent
	 * @return
	 */
	public Position moveS(Position curent) {
		int x = curent.getX();
		int y = curent.getY() - 1;
		
		if (y < 0) {
			return null;
		}
		return new Position(x, y);
	}
	
	/**
	 * Next move to the West
	 * @param curent
	 * @return
	 */
	public Position moveV(Position curent) {
		int x = curent.getX() - 1;
		int y = curent.getY();
		
		if (x < 0) {
			return null;
		}
		return new Position(x, y);
	}
	
	/**
	 * Next Move to the East
	 * @param curent
	 * @return
	 */
	public Position moveE(Position curent) {
		int x = curent.getX() + 1;
		int y = curent.getY();
		
		if (x >= m) {
			return null;
		}
		return new Position(x, y);
	}
	
	/**
	 * Is this position known as a swamp
	 * @param p
	 * @return
	 */
	public boolean isKSwamp(Position p) {
		return swamps.contains(p);
	}
	
	/**
	 * Is known as a secure position
	 * @param p
	 * @return
	 */
	public boolean isKSecure(Position p) {
		return secure.contains(p);
	}
	
	/**
	 * is Known wall position
	 * @param p
	 * @return
	 */
	public boolean isKWall(Position p) {
		return walls.contains(p);
	}
	
	/**
	 * is known unsecure position
	 * @param p
	 * @return
	 */
	public boolean isKUnsecure(Position p) {
		return unsecure.contains(p);
	}
	
	/**
	 * Was this position already visited
	 * @param p
	 * @return
	 */
	public boolean isVisited(Position p) {
		return (secure.contains(p) || walls.contains(p) ||
				swamps.contains(p));
	}
	
	public boolean checkSmell(Position p) {
		Position north = this.moveN(p);
		Position south = this.moveS(p);
		Position east = this.moveE(p);
		Position west = this.moveV(p);
		
		boolean snorth = false, ssouth = false, seast = false, swest = false;
		
		if(north != null)
			if (worldmap[north.getY()][north.getX()] == 'm') {
				snorth = true;
			}
		
		if(south != null)
			if (worldmap[south.getY()][south.getX()] == 'm') {
				
				ssouth = true;
			}
		
		if (east != null)
			if (worldmap[east.getY()][east.getX()] == 'm') {
				
				seast = true;
			}
		
		if (west != null)
			if (worldmap[west.getY()][west.getX()] == 'm') {
				
				swest = true;
			}
		
		if(snorth || seast || ssouth || swest) {
			int nr_visited = 0;
			if (!isVisited(south)) {
				unsecure.add(south);
			} else {
				nr_visited++;
			}
			
			if (!isVisited(north)) {
				unsecure.add(north);
			} else {
				nr_visited++;
			}
			
			if (!isVisited(east)) {
				unsecure.add(east);
			} else {
				nr_visited++;
			}
			
			if (!isVisited(west)) { 
				unsecure.add(west);
			} else {
				nr_visited++;
			}
			if (nr_visited == 3)
			
			return true;
		}
		return false;
	}
	
	public boolean attemptMove(Position next) {
		char c = worldmap[next.getY()][next.getX()];
		if (c == 'p') {
			return false;
		}
		return true;
	}
	
	public boolean executeMove(Position curent, Position next) {
		if (!(isVisited(next))) {
			if (checkSmell(next)) {
				secure.add(next);
			} else {
				if (attemptMove(next)) {
					curentRoad.addLast(next);
					curent = next;
					secure.add(next);
					return true;
				} else {
					walls.add(next);
					return false;
				}
			}
		}
		return false;
	}
	
	public void explore() {
		Position curent = this.findinit();
		if (curent == null) {
			return;
		}
		curentRoad.addLast(curent);
		while(!curentRoad.isEmpty()) {
			Position nextN = this.moveN(curent);
			boolean exN = executeMove(curent, nextN);
			if (exN) {
				continue;
			}
			
			Position nextE = this.moveE(curent);
			boolean exE = executeMove(curent, nextE);
			if (exE) {
				continue;
			}
			
			Position nextS = this.moveS(curent);
			boolean exS = executeMove(curent, nextS);
			if (exS) {
				continue;
			}
			
			Position nextV = this.moveV(curent);
			boolean exV = executeMove(curent, nextV);
			if (exV) {
				continue;
			}
			
			curent = curentRoad.getLast();
			curentRoad.removeLast();
		}
		
	}
	
	public Position get_max_it(Iterator it, Position max) {
		while (it.hasNext()) {
			Position p = (Position) it.next();
			if (max.getX() < p.getX()) {
				max.setX(p.getX());
			}
			if (max.getY() < p.getY()) {
				max.setY(p.getY());
			}
		}
		return max;
	}
	
	public Position get_max() {
		Integer maxx = -1, maxy = -1;
		Position max = new Position(-1, -1);
		get_max_it(secure.iterator(), max);
		get_max_it(unsecure.iterator(), max);
		get_max_it(walls.iterator(), max);
		get_max_it(swamps.iterator(), max);
		return max;
	}
	
	public void build_map() {
		explore();
		Position max = get_max();
		System.out.println(max.getX() + " " + max.getY());
		this.robotmap = new char[n+1][m+1];
		for(int i = 0; i <= max.getY(); i++) {
			for (int j = 0; j <= max.getX(); j++) {
				robotmap[i][j] = '+';
			}
		}
		Iterator it = swamps.iterator();
		while (it.hasNext()) {
			Position p = (Position) it.next();
			robotmap[p.getY()][p.getX()] = 'm';
		}
		it = walls.iterator();
		while (it.hasNext()) {
			Position p = (Position) it.next();
			robotmap[p.getY()][p.getX()] = 'p';
		}
		it = unsecure.iterator();
		while (it.hasNext()) {
			Position p = (Position) it.next();
			robotmap[p.getY()][p.getX()] = 'u';
		}
		it = secure.iterator();
		while (it.hasNext()) {
			Position p = (Position) it.next();
			robotmap[p.getY()][p.getX()] = 's';
		}
		Position init = findinit();
		robotmap[init.getY()][init.getX()] = 'i';
		for (int i = 0; i < robotmap.length; i++) {
			for (int j = 0; j < robotmap.length; j++) {
				System.out.print(robotmap[i][j]);
			}
			System.out.println();
		}
	}
	
}
