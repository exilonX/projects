
public class Position {
	private Integer x, y;
	
	public Position() {
		x = 0;
		y = 0;
	}

	public Position(Integer x, Integer y) {
		this.x = x;
		this.y = y;
	}
	
	public Integer getY() {
		return y;
	}

	public void setY(Integer y) {
		this.y = y;
	}

	public Integer getX() {
		return x;
	}

	public void setX(Integer x) {
		this.x = x;
	}
	
	@Override
	public boolean equals(Object obj) {
		if ((this.x == ((Position)obj).x) && (this.y == ((Position)obj).y)) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public int hashCode() {
		return x + y;
	}
	
	@Override
	public String toString() {
		return String.valueOf(x) + " " + String.valueOf(y);
	}
}
