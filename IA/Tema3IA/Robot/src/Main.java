import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;


public class Main {
	
	public static char[][] readmap(String filename) {
		BufferedReader br;
		char[][] map = null;
		try {
			br = new BufferedReader(new FileReader(filename));
			String line;
			line = br.readLine();
			String[] splitted = line.split(" ");
			int n = Integer.parseInt(splitted[0]);
			int m = Integer.parseInt(splitted[1]);
			System.out.println(n + "  " + m);
			map = new char[n][m];
			int i = 0, j = 0;
			while ((line = br.readLine()) != null) {
			   splitted = line.split(" ");
			   j = 0;
			   
			   for (int k = 0; k < splitted.length; k++) {
				   map[i][j++] = splitted[k].charAt(0);
			   }
			   
			   ++i;
			}
			br.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return map;
	}
	
	public static void main(String[] args) {
		char[][] map = null;
		Integer n= 0, m = 0;
		String filename = "test1.txt";
		map = readmap(filename);
		
		for (int i = 0; i < map.length; i++) {
			for (int j = 0; j < map[0].length; j++) {
				System.out.print(map[i][j]);
			}
			System.out.println();
		}
		ExplorerRobot robot = new ExplorerRobot(map);
		robot.build_map();
	}

}
