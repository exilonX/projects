insert(E, [], [E]).
insert(E, [H | Rest], [E, H | Rest]) :- E < H, ! .
insert(E, [H | Rest], [H | Rez]) :- insert(E, Rest, Rez).


% isort([], []).
% isort([X], [X]).
% isort([H1, H2 | T], [H1 | Rez]) :- H1 < H2, isort([H2 | T], Rez).
% isort([H1, H2 | T], [H2 | Rez]) :- H1 >= H2, isort([H1 | T], Rez).


isort([], []).
isort([H | T], S) :- isort(T, STail), insert(H, STail, S).

